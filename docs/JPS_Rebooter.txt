JPS Rebooter 

Features:
1. server_status_check() - returns the status of a server and its service
	Statuses:
	- Offline
	- Server_Online, Service_Online
	- Server_Online, Service_Offline
	server_check()
	service_check()

2. server_reboot() - reboots a server
3. cluster_check() - ensures two other JPS servers in a cluster are up

References:
defect_counter

def get_report(book_name, report_shape):
    try:
        book = openpyxl.load_workbook(book_name)
    except FileNotFoundError:
        print('Creating file {}'.format(book_name))
        book = create_report(book_name, report_shape)
    except PermissionError:
        print('File {} is open. Please close file.'.format(book_name))
        return None
    except InvalidFileException:
        print('Please use a .xlsx file extension for file {}.'.format(book_name))
        return None
    return book