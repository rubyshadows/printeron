# Install wmi https://pypi.org/project/WMI/

import wmi
from printeron.credentials import MY_GSM_USER, MY_PASS

def connect(server):
    try:
        conn = wmi.WMI(server+".gsm1900.org", user=MY_GSM_USER, 
                        password=MY_PASS)
        return conn
    except Exception as e:
        print(e)
        return None

def reboot(conn):
    try:
        os = conn.Win32_OperatingSystem (Primary=1)[0]
        os.Reboot()
    except Exception as e:
        print(e)

def connect_test():
    server = 'PRDPWRMPR0018'
    conn = connect(server)
    # Print some stuff about the connection

def reboot_test():
    server = 'PRDPWRMPR0018'
    conn = connect(server)
    reboot(conn)

if __name__ == '__main__':
    reboot_test()
    #connect_test()
    #madison, reboot PRDPWRMPR005A
