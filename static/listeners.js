window.onload = function() {
    
    function hideAllLoading() {
        let eles = document.getElementsByClassName("loading")
        for (let ele of eles){
            ele.style["display"] = "none"
        }
    }

    function showLoading(id){
        hideAllLoading()
        let ele = document.getElementById(id)
        console.log(ele)
        ele.style["display"] = "block"

    }


    let getSync = document.getElementById("getSync");
    getSync.addEventListener("click", function (evnt) {
		showLoading("getSync_span")
	});
}