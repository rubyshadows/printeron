from datetime import datetime
import json

import pytz # pip3 install --user pytz
import dateutil.parser # pip3 install --user python-dateutil
import requests

import printeron.ponmanager_requests_login as pml
import printeron.printer_counter as pc

'''
Log into all servers and data collect:
1. Function that collects the last sync date for all servers (almost-done)
- One by one, log into each server 
- Capture the page with the last sync date
- Extract the date from that page
- Add date and server to a list of dictionries [{server: datetime}] (here)
    -- May want to use the sqllite package (here)
- Return a list of dicts 
-- Errors:
    -- Login Timeout: Can't log in or reach the log in page, either because not on VPN or because of some other unknown factor
        -- Add the failed server to a retry list [server]
        -- Do a retry pass through, before returning the list of dicts
2. Create a data table: Server - Last Sync DateTime - Scheduled Sync DateTime - Status (easy)
3. Create a DateTimeDict Queue: [{last_sync datetime, scheduled_sync, printer_count, server, status}] <to be checked every Delta minutes> (easy)
4. Create an algorithm which assigns staggered sync times to the servers in the queue (easy)
5. Create a targetting algorithm, which collects servers with Scheduled Sync between current Delta and Previous Delta (medium)
6. Create Algorithm that changes the status of a set of servers in Queue (easy)
7. Function that updates the Queue in SQL file, using server name as ID (easy)

8. Algorithm that logs into the target servers and run the synchronize button (medium)
9. Flask app that prints the database to UI (easy)

Extra: 
    - Create a script which syncs all servers in a staggered way
    - Create a script that does the sync every 2 hours 
    - Then finish the Flask reporting
'''

# Links to the time last synced for each server
TZINFOS = { 'PDT': pytz.timezone('US/Pacific')}

SERVERS = {
            '​prdasrmpr001': 'https://prdasrmpr001:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598382851136',
            'prdasrmpr002': 'https://prdasrmpr002:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598366683174', 
            'prdpwrmpr0007': 'https://prdpwrmpr0007:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598378459236',
            'prdpwrmpr001a': 'https://prdpwrmpr001a:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598383727228', 
            'prdpwrmpr001b': 'https://prdpwrmpr001b:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598384857231', 
            'prdpwrmpr001c': 'https://prdpwrmpr001c:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598384929919',
            'prdpwrmpr005a': 'https://prdpwrmpr005a:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598366351161', 
            'prdpwrmpr005b': 'https://prdpwrmpr005b:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385023745', 
            'prdpwrmpr005c': 'https://prdpwrmpr005c:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385102844',
            'prdpwrmpr0010': 'https://prdpwrmpr0010:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385160511', 
            'prdpwrmpr0011': 'https://prdpwrmpr0011:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385232994', 
            'prdpwrmpr0012': 'https://prdpwrmpr0012:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385281477',
            'prdpwrmpr006d': 'https://prdpwrmpr006d:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385332034', 
            'prdpwrmpr006e': 'https://prdpwrmpr006e:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385377267', 
            'prdpwrmpr006f': 'https://prdpwrmpr006f:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385432734',
            'prdpwrmpr0070': 'https://prdpwrmpr0070:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385501720', 
            'prdpwrmpr0071': 'https://prdpwrmpr0071:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385683814', 
            'prdpwrmpr0072': 'https://prdpwrmpr0072:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385746796',
            'prdasrmpr601': 'https://prdasrmpr601:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385827208', 
            'prdasrmpr602': 'https://prdasrmpr602:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385881279', 
            'prdtwrmpr0005': 'https://prdtwrmpr0005:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385929735',
            'prdtwrmpr002a': 'https://prdtwrmpr002a:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598385975562', 
            'prdtwrmpr002b': 'https://prdtwrmpr002b:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386021501',
            'prdtwrmpr002c': 'https://prdtwrmpr002c:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386064246',
            'prdtwrmpr004a': 'https://prdtwrmpr004a:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386110227', 
            'prdtwrmpr004b': 'https://prdtwrmpr004b:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386164232',
            'prdtwrmpr004c': 'https://prdtwrmpr004c:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386310651',
            'prdtwrmpr0022': 'https://prdtwrmpr0022:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386357373', 
            'prdtwrmpr0023': 'https://prdtwrmpr0023:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386410752', 
            'prdtwrmpr0024': 'https://prdtwrmpr0024:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386467976',
            'prdtwrmpr0085': 'https://prdtwrmpr0085:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386646574', 
            'prdtwrmpr0086': 'https://prdtwrmpr0086:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598386997100', 
            'prdtwrmpr0087': 'https://prdtwrmpr0087:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598387176252',
            'prdtwrmpr0088': 'https://prdtwrmpr0088:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598387446855', 
            'prdtwrmpr0089': 'https://prdtwrmpr0089:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598388321631', 
            'prdtwrmpr008a': 'https://prdtwrmpr008a:8057/printers/list?printerClass=%5BPUBLIC%2C+IPP%5D&_=1598388384994'
            }
PRINTER_COUNT_LINKS = pc.SERVERS

LAST_SYNCS = []

def extract_all_last_syncs(servers = SERVERS):
    '''
        Retrieve last_sync date for all servers in the array
    '''
    servers = list(servers.keys())
    retry_list = []
    return_list = []
    for server in servers:
        try:
            client = pml.pon_login(servers=[server])
            if not client: 
                retry_list += server
                continue
            printer_url = SERVERS.get(server)
            count_url = PRINTER_COUNT_LINKS.get(server)
            last_sync = extract_last_sync(client, printer_url)
            count = extract_count(client, count_url)
            server_dict = {'server': server, 'last_sync':last_sync, 'printer_count':count}
            return_list.append(server_dict)
            print(server_dict)
        except Exception as e:
            retry_list += server
            print('exception in extract all for {}.\n{}'.format(server, e))
    return return_list

def extract_count(client, count_url):
    '''
        Use established connection to retrieve printer count for CPS server
    '''
    json_resp = client.get(count_url, allow_redirects=True, verify=False, timeout=10)
    json_data = json.loads(json_resp.text)
    count = json_data.get('recordsTotal')
    return count

def extract_last_sync(client, printer_url):
    '''
        Use established connection to retrieve last_sync date
    '''

    r = client.get(printer_url, allow_redirects=True, verify=False, timeout=10)  # sets cookie
    html = r.text
    mark_start = '<input id="lastSynch-string" type="hidden" value="'
    mark_end = '"/>'

    start_index = html.find(mark_start) + len(mark_start)
    end_index = html.find(mark_end, start_index)

    last_sync_string = html[start_index:end_index]
    datetime_obj = dateutil.parser.parse(last_sync_string, tzinfos=TZINFOS)
    #print(datetime_obj)

    #print('last sync for {} is {}'.format(printer_url, last_sync_string))
    return last_sync_string

if __name__ == '__main__':
    extract_all_last_syncs()