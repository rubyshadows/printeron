from inspect import currentframe as cf
import re

class HealthUtility:
    DEBUG_LEVEL = 0

    @staticmethod
    def debug(func_name, msg, lvl):
        if HealthUtility.DEBUG_LEVEL >= lvl: print('{}: {}'.format(func_name, msg))

    @staticmethod
    def check_sap_form(sap):
        if not sap:
            return False
        digits = 0
        chars = 0
        for c in sap:
            if c.isdigit():
                digits += 1
            elif c.isalpha():
                chars += 1
            else:
                pass
        if digits > chars and digits >= 3:
            return True
        else:
            return False

    @staticmethod
    def sap_from_description(desc):
        return HealthUtility.sap_from_worklog([{'LogText':desc}])

    @staticmethod
    def sap_regex(worklogs): 
        #TODO: Only return log when number is found as well
        # Perform first bit of sap_from_worklog algorithm

        def find_number(text, log, found):
            sap_idx = log.find(found) # get index if found
            num_idx = None
            after_sap = text[sap_idx:] #recapitalize
            parenthesis = False
            for c in after_sap:
                if c == ')': parenthesis = False
                if c == '(': parenthesis = True
                if parenthesis: continue
                if c.isdigit():
                    num_idx = after_sap.find(c)
                    break
            if not num_idx: return False
            else: return True

        for l in worklogs:
            text = l['LogText'].replace('\n', ' ')
            log = text.lower()
            regex = r'[^a-zA-Z0-9]sap[^a-zA-Z0-9]'
            #regex2 = r'sap[^a-zA-Z0-9]'
            match = re.search(regex, log)
            #match2 = re.search(regex2, log)
            HealthUtility.debug(cf().f_code.co_name, '{}'.format(log), 1)
            if match:
                t = re.findall(regex, log)[0]
                HealthUtility.debug(cf().f_code.co_name, 'found sap {}'.format(t), 1)
                if find_number(text, log, t):
                    return text, log, t
            # elif match2:
            #     tt = re.findall(regex2, log)[0]
            #     debug_3('sap search 2: found sap {}'.format(tt))
            #     return text, log, tt
        return None, None, None

    @staticmethod
    def sap_from_worklog(worklogs):
        log = None
        found = None
        text = None
        # for l in worklogs: # find match 
        #     text = l['LogText'].lower()
        #     if text.count('sap ') or text.count('sap:'):
        #         log = text
        #         if text.count('sap '):
        #             found = 'sap '
        #         elif text.count('sap:'):
        #             found = 'sap: '
        #         break
        text, log, found = HealthUtility.sap_regex(worklogs)
        if not found: return None
        sap_idx = log.find(found) # get index if found
        num_idx = None
        after_sap = text[sap_idx:] #recapitalize
        parenthesis = False
        for c in after_sap:
            if c == ')': parenthesis = False
            if c == '(': parenthesis = True
            if parenthesis: continue
            if c.isdigit():
                num_idx = after_sap.find(c)
                break
        if not num_idx: return None
        
        start_idx = num_idx
        end_idx = num_idx
        while(after_sap[start_idx].isalnum() and start_idx >= 0):
            start_idx -= 1
        start_idx += 1
        while(after_sap[end_idx].isalnum() and end_idx < len(after_sap)):
            end_idx += 1
        sap = after_sap[start_idx: end_idx]
        HealthUtility.debug(cf().f_code.co_name, 'Sap in worklog - {}'.format(sap), 1)
        return sap    