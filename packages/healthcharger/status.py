from packages.database.sqlite import SQLite

class Status:
    DB_FILE =               'TReports.db'
    DB_TABLE =              'Status'
    DB_SAP_TABLE =          'Sap_Status'
    STATUS_COLUMNS =        {'id': 'INTEGER', 'message': 'TEXT'}
    SAP_STATUS_COLUMNS =    {'sap': 'TEXT', 'status_id': 'INTEGER', 'port': 'TEXT', 'switch': 'TEXT', 'mac_minis': 'INTEGER'}

    NO_ABNORMALITIES = 1
    TBUILDER_OUTDATED = 2
    PRINTERON_OUTDATED = 3
    STOREHEALTH_OUTDATED = 4
    TBUILDER_LOGIN_FAILURE = 5
    TBUILDER_SAP_NOT_FOUND = 6
    SWITCH_CANNOT_CONNECT = 7
    SWITCH_WRONG_STORE = 8
    SWITCH_MINI_WRONG_PORT = 9
    SWITCH_MINI_EXTERNAL = 10
    SWITCH_MINI_NOT_FOUND = 11
    QFT_MINI_NOT_FOUND = 12
    QFT_CHECKING_IN_BUT_MINI_NOT_FOUND = 13

    STATUS_MAP = {
        NO_ABNORMALITIES: 'No Abnormalities', # Supply connected port
        TBUILDER_OUTDATED: 'TBuilder - IP Outdated',
        PRINTERON_OUTDATED: 'PrinterOn - IP Outdated',
        STOREHEALTH_OUTDATED: 'StoreHealth - IP Outdated',
        TBUILDER_LOGIN_FAILURE: 'TBuilder - Login Failure',
        TBUILDER_SAP_NOT_FOUND: 'TBuilder - SAP Not Found',
        SWITCH_CANNOT_CONNECT: 'Switch - Cannot Connect',
        SWITCH_WRONG_STORE: 'Switch - Connected To The Wrong Store', # Supply wrong sap
        SWITCH_MINI_WRONG_PORT: 'Switch - Mac Mini Found Wrong Port', # Supply connected port
        SWITCH_MINI_EXTERNAL: 'Switch - Mac Mini On External Switch', # Supply connected port
        SWITCH_MINI_NOT_FOUND: 'Switch - No Mac Mini Found',
        QFT_MINI_NOT_FOUND: 'QuickFix - No Mini Found', # Supply sap
        QFT_CHECKING_IN_BUT_MINI_NOT_FOUND: 'QuickFix - Mac Mini Checking In But Not Found On Switch'
    }

    @staticmethod
    def provision():
        CONN = SQLite.create_sqlite(Status.DB_FILE)
        SQLite.create_table(CONN, Status.DB_TABLE, Status.STATUS_COLUMNS)
        statuses = [{'id': k, 'message': v} for k,v in Status.STATUS_MAP.items()]
        for status in statuses:
            SQLite.insert_or_update_rows(CONN, Status.DB_TABLE, status, {'id': status.get('id')})

        SQLite.create_table(CONN, Status.DB_SAP_TABLE, Status.SAP_STATUS_COLUMNS)

    @staticmethod
    def insert_or_update(sap, status=None, port=None, switch=None, mac_minis=None):
        '''
            Inserts entry into the Sap_Status table
            @sap: Store # which is the key for the record
            @status: One of the predefined statuses
            @port: Port that the mini is connected on, if connected
            @switch: Internal or external, if connected
            @mac_minis: Number of mac minis at the store
        '''
        CONN = SQLite.create_sqlite(Status.DB_FILE)
        if status and status not in Status.STATUS_MAP: status = None
        SQLite.insert_or_update_rows(CONN, Status.DB_SAP_TABLE, {'sap': sap, 'status_id': status, 'port': port, 'switch': switch, 'mac_minis': mac_minis}, {'sap': sap})


    @staticmethod
    def test_parameters(test=NO_ABNORMALITIES):
        if test == Status.NO_ABNORMALITIES:
            print ('Success')
        else:
            print ('Not equal')

if __name__ == '__main__':
    CONN = SQLite.create_sqlite(Status.DB_FILE)
    SQLite.drop_table(CONN, Status.DB_TABLE)
    SQLite.drop_table(CONN, Status.DB_SAP_TABLE)
    Status.provision()
    rows = SQLite.select_rows(CONN, Status.DB_TABLE)
    for row in rows:
        r = dict(row)
        for k,v in r.items():
            print('{}: {}'.format(k, v))
        print()