import json

import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from tqdm import tqdm

from packages.database.sqlite import SQLite 
from packages.healthcharger.status import Status

'''
TODO:
1. Need HealthUtility.build_name function re-implemented
'''

class QuickFix:
        
    DB_TABLE = 'QuickFix'
    DB_ERR_TABLE = 'QuickFix_Errors'
    DB_ERR_COLUMNS = {'sap': 'text'}
    DB_ERR_FILE = 'QuickFix_Errors.csv'
    DB_CSV_FILE = 'QuickFix.csv'
    
    DEBUG_LEVEL = 0
    DEBUG_CRITICAL = 1
    DEBUG_MAJOR = 2
    DEBUG_INFO = 3
    DEBUG_DEBUG = 4

    QFT_LINK = 'https://mi-qft.geo.cf.t-mobile.com/background_process?store_search={}'

    @staticmethod
    def get_qft_data(sap, DB_FILE = 'TReports.db', TABLE = 'QuickFix',
                     ERR_TABLE = None):

        CONN = SQLite.create_sqlite(DB_FILE)
        ERR_COLUMNS = {'sap': 'text'}
        if not ERR_TABLE: ERR_TABLE = TABLE + '_Errors'
        SQLite.create_table(CONN, ERR_TABLE, ERR_COLUMNS)
        sap_search = {'sap': sap}
        
        try:
            res = requests.get(QuickFix.QFT_LINK.format(sap), verify=False)
            results = json.loads(res.text).get('result').get('mini_health')
            mini_health = json.loads(res.text).get('result').get('mini_health')[0]
            reply = {'sap': sap}
            if type(mini_health) == str: raise Exception('QFT error for sap {}'.format(sap))
            for mini in results:
                if mini.get('name')[-2:] == '01': mini_health = mini
            reply['status'] = mini_health.get('check_in_status')
            reply['name'] = mini_health.get('name')
            reply['last_check_in'] = mini_health.get('last_check_in_utc')
            reply['model'] = mini_health.get('model')
            reply['serial'] = mini_health.get('serial')
            reply['mac_address'] = mini_health.get('mac_address')
            reply['ac2_version'] = mini_health.get('ac2_version')
            reply['apu_version'] = mini_health.get('apu_version')
            reply['os_version'] = mini_health.get('os_version')
            
            # Cache results in sqlite database
            new_columns = dict([(key, 'text') for key in reply])
            SQLite.create_table(CONN, TABLE, new_columns)
            SQLite.insert_or_update_rows(CONN, TABLE, columns=reply, where=sap_search)
            SQLite.delete_rows(CONN, ERR_TABLE, sap_search)

            return reply
        except Exception as e:
            print('QuickFix: ', str(e))
            SQLite.insert_or_update_rows(CONN, ERR_TABLE, sap_search, sap_search)
            # Add failures to QFT_Sap_Failures table

    @staticmethod
    def unpad_sap(sap):
        if not sap: return None
        count = 0
        for i in sap:
            if i != '0': break
            count += 1
        return sap[count:]

if __name__ == '__main__':
    DB_FILE = 'TReports.db'
    TABLE = 'QuickFix'
    ERR_TABLE = TABLE + '_Errors' # Delete row if connect success
    ERR_COLUMNS = {'sap': 'text'}
    ERR_FILE = ERR_TABLE + '.csv'
    CSV_FILE = TABLE + '.csv'
    CONN = SQLite.create_sqlite(DB_FILE)
    SQLite.drop_table(CONN, TABLE)
    SQLite.drop_table(CONN, ERR_TABLE)
    saps = ['985E', '976E', '955D', '5309']
    reports = []
    for i in tqdm(range(len(saps))):
        sap = saps[i]
        qft_report = QuickFix.get_qft_data(sap)
        reports.append(qft_report)

    SQLite.sql_to_csv(CONN, CSV_FILE, TABLE)
    SQLite.sql_to_csv(CONN, ERR_FILE, ERR_TABLE)

    for report in reports:
        print(report)