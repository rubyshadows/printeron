import json

from fabric.connection import Connection
from tqdm import tqdm 

from packages.utility.utility import Utility as UTIL
from packages.database.sqlite import SQLite
from packages.healthcharger.status import Status

class Switch:

    DEFAULT_OCTET = '0'
    INTERNAL_OCTET = '6'
    EXTERNAL_OCTET = '10'
    ROUTER_OCTET = '254'
    COMMAND_DESC = 'sho int desc'
    COMMAND_SECURE_PORTS = 'sho port'
    COMMAND_MAC_ADDR = 'sho mac add int {}'
    COMMAND_MAC_TABLE = 'sho mac addr'
    DEBUG_LEVEL = 0
    DEBUG_CRITICAL = 1
    DEBUG_MAJOR = 2
    DEBUG_INFO = 3
    DEBUG_DEBUG = 4
    DB_FILE = 'TReports.db'
    DB_TABLE = 'Port15_Statuses'
    DB_ERR_TABLE = DB_TABLE + '_Errors'
    DB_ERR_COLUMNS = {'ip_address': 'text'}

    TEST_USERNAME = ''
    TEST_PASSWORD = ''

    @staticmethod
    def check_interface(interface):
        return interface.lower().count('fa') or interface.lower().count('gi')

    @staticmethod
    def convert_mac_addr(mac_addr):
        '''
            Converts mac address from ####.####.####
            to ##:##:##:##:##:##
        '''
        if not mac_addr: return None
        nums = mac_addr.split('.')
        new_addr = ""
        delimiter = ''
        for num in nums:
            for i in range(0,4,2):
                new_addr += delimiter
                new_addr += num[i:i+2]
                delimiter = ':'
        return new_addr

    @staticmethod
    def count_violating_ports(conn):
        out = Switch.run(conn, Switch.COMMAND_SECURE_PORTS)
        lines = out.split('\n')
        clean_lines = [x.split() for x in lines]
        clean_lines = [x for x in clean_lines if len(x) == 5]
        
        violating_interfaces = []
        [violating_interfaces.append(line[0]) for line in clean_lines if line[3] != '0']
        return violating_interfaces # can use length of interfaces to get the count

    @staticmethod
    def create_dict(conn):
        desc = conn.run(Switch.COMMAND_DESC, hide=True)
        new_desc = Switch.cut_disclaimer(desc.stdout)
        desc_split = new_desc.split('\n')
        port_dict = {}
        for set in desc_split:
            vals = set.split()
            if len(vals) == 0 or vals[0] == 'Interface': continue
            inf = vals[0]
            status = vals[1]
            protocol = vals[2]
            description = ""
            if status == 'admin': 
                status = vals[1] + ' ' + vals[2]
                protocol = vals[3]
                if len(vals) > 4:
                    description += vals[4]
                    for i in range(5, len(vals)): description += " {}".format(vals[i])
            elif len(vals) > 3:
                description += vals[3]
                for i in range(4, len(vals)): description += " {}".format(vals[i])
            else:
                description = None
                
            port_dict[inf] = {}
            port_dict[inf]['interface'] = inf
            port_dict[inf]['status'] = status
            port_dict[inf]['protocol'] = protocol
            port_dict[inf]['description'] = description
            if inf.count('15') > 0 and not port_dict.get('port15'): port_dict['port15'] = port_dict[inf] #smells
        return port_dict

    @staticmethod
    def create_dict_heavy(conn):
        try:
            port_dict = Switch.create_dict(conn)
        except:
            Switch.debug('create_dict_heavy: retrying', Switch.DEBUG_INFO)
            port_dict = Switch.create_dict(conn)

        try:
            port_dict = Switch.populate_mac_addrs(conn, port_dict)
        except:
            Switch.debug('create_dict_heavy: retrying', Switch.DEBUG_INFO)
            port_dict = Switch.populate_mac_addrs(conn, port_dict)

        try:
            Switch.populate_violations(conn, port_dict)
        except:
            Switch.debug('create_dict_heavy: retrying', Switch.DEBUG_INFO)
            Switch.populate_violations(conn, port_dict)
        return port_dict

    @staticmethod
    def cut_disclaimer(output): # can test on all stores with stores.txt
        end = 'action.'
        if output.count('action.') < 1:
            end = '7326'
        elif output.count('7326') < 1:
            return output
        idx = output.find(end)
        new_out = output[idx+len(end):]
        return new_out

    @staticmethod
    def debug(msg, lvl):
        if Switch.DEBUG_LEVEL >= lvl:
            print(msg)

    @staticmethod
    def debug_func(func, lvl):
        if Switch.DEBUG_LEVEL >= lvl:
            func()

    @staticmethod
    def error_log(sap, error_code):
        '''
            Log error status for a sap
            @sap: Sap to log
            @error_codes: Status Error Code
                Status.SWITCH_CANNOT_CONNECT = 7
                Status.SWITCH_WRONG_STORE = 8
                Status.SWITCH_MINI_WRONG_PORT = 9
                Status.SWITCH_MINI_EXTERNAL = 10
                Status.SWITCH_MINI_NOT_FOUND = 11
        '''
        pass

    @staticmethod
    def error_retry(user, pw, where = {}, DB_FILE = DB_FILE, TABLE = DB_TABLE, ERR_TABLE = DB_ERR_TABLE):
        '''
            Retries all of the ip addresses in the error table
            @user: nt username
            @pw: nt password
            @where: where clause to filter ips by
            @DB_FILE: sqlite database file
            @TABLE: table with successful reports
            @ERR_TABLE: table with ip addresses of unsuccessful reports
        '''
        CONN = SQLite.create_sqlite(DB_FILE)
        rows = SQLite.select_rows(CONN, ERR_TABLE, columns={}, where=where)
        ips = []
        if len(rows):
            for row in rows:
                ips.append(row['ip_address'])
        for ip in ips:
            Switch.run_report(user, pw, ip, DB_FILE = DB_FILE, TABLE = TABLE, ERR_TABLE = ERR_TABLE)

    @staticmethod
    def find_mac_addr(conn, inf):
        '''
            Finds the mac address connected on the port
            @conn: connection with proprer credentials and ip
            @inf: interface of port to check
        '''
        cmd = Switch.COMMAND_MAC_ADDR.format(inf)
        try:
            out = Switch.run(conn, cmd) # can fail and needs to retry / update errors
        except:
            Switch.debug('find_mac_addr: retrying', Switch.DEBUG_INFO)
            out = Switch.run(conn, cmd)
        switch_jamf = Switch.parse_mac_addr_signature(out)
        if not switch_jamf:
            switch = None
            jamf = None
        else:
            switch, jamf = switch_jamf
        return switch, jamf

    @staticmethod
    def find_mini_ports(conn):
        '''
            Finds the ports provisioned for mac mini usage
            @conn: Connection with proper credentials and ip address
            Returns the ports with description containing "mac mini content server"
        '''
        patterns = ['MAC Mini Content Server']
        correct_ports = []
        Switch.debug_func(lambda :print('0 ', end=''), Switch.DEBUG_INFO)
        try:
            port_dict = Switch.create_dict_heavy(conn)
        except:
            print('0.5 ', end='')
            port_dict = Switch.create_dict_heavy(conn)

        Switch.debug_func(lambda :print('1 ', end=''), Switch.DEBUG_INFO)
        
        port_dict.pop('violations', None)
        port_dict.pop('port15', None)

        for entry in port_dict.values():
            interface = entry.get('interface')
            desc = entry.get('description')
            
            for pattern in patterns:
                if desc and pattern.lower() in desc.lower():
                    try:
                        mac_switch, mac_jamf = Switch.find_mac_addr(conn, interface)
                    except:
                        mac_switch, mac_jamf = Switch.find_mac_addr(conn, interface)
                    correct = 'N/A'
                    if mac_switch:
                        if mac_switch   and Switch.is_mini(mac_switch):     correct = 'True'
                        elif mac_switch and not Switch.is_mini(mac_switch): correct = 'False'
                    correct_ports.append({'port':interface,     'mac_addr':mac_jamf, 
                                          'description':desc,   'correct': correct})
        Switch.debug_func(lambda :print('2'), Switch.DEBUG_INFO)
        return correct_ports

    @staticmethod
    def find_minis(conn):
        '''
            Finds all mac minis on the switch
            @conn: Connection with proper credentials and ip address
            Returns the port of each, or empty dict if none
        '''
        import re
        # re.sub(' +', ' ', 'The     quick brown    fox')
        patterns = ['787b', '9810', 'f018', 'a860', '68fe']
        def port_shortname(long_port):
            g = long_port.replace('GigabitEthernet', 'Gi')
            f = g.replace('FastEthernet', 'Fa')
            return f
        cmd = Switch.COMMAND_MAC_TABLE
        minis = []
        try:
            out = Switch.run(conn, cmd) # can fail and needs to retry / update errors
        except:
            Switch.debug('mac table: retrying', Switch.DEBUG_INFO)
            out = Switch.run(conn, cmd)
        lines = out.splitlines()
        for i in range(len(lines)):
            line = lines[i]
            for pat in patterns:
                if pat in line:
                    re_space = re.sub(' +', ' ', line)
                    elements = re_space.split()
                    mac_addr = elements[1]
                    ip_type = elements[2].upper()
                    port = port_shortname(elements[-1])
                    mini = {'mac_addr':mac_addr, 'ip_type':ip_type, 'port':port}
                    minis.append(mini)
                    # print('{}: {} {} {}'.format(i, mac_addr, ip_type, port))
        return minis

    @staticmethod
    def is_mini(mac_addr):
        '''
            Decides if address belongs to a mac mini
            @mac_addr: mac address
            Returns True or False
        '''
        if not mac_addr: return False
        split = mac_addr.split('.')
        if len(split) < 3: return False
        prefix = split[0]
        import re
        patterns = ['787b', '9810', 'f018', 'a860', '68fe']
        if prefix.lower() in patterns: return True
        else: return False

    @staticmethod
    def make_host_string(user, domain):
        if not user or not domain: return None
        return user + '@' + domain

    # @staticmethod
    # def parse_mac_addr(string): # too fragile
    #     lines = string.split('\n')
    #     #print('length of mac list: {}'.format(len(lines)))
    #     penultimate = lines[-2].split()
    #     if len(penultimate) > 1 and penultimate[1].count('.') > 1:
    #         mac_addr = penultimate[1]
    #         mac_addr_jamf = Switch.convert_mac_addr(mac_addr)
    #         Switch.debug('HealthUtility: parse_mac_addr returning {}'.format(mac_addr_jamf),3)
    #         return mac_addr, mac_addr_jamf
    #     else:
    #         Switch.debug('HealthUtility: parse_mac_addr returning None', 3)
    #         return None

    @staticmethod
    def parse_mac_addr_signature(string):
        lines = string.splitlines()
        for i in range(len(lines)):
            line = lines[i]
            if 'type' in line.lower():
                j = i + 2
                if j >= len(lines): return None, None
                mac_line = lines[j]
                if '.' in mac_line:
                    import re
                    mac_line_single_space = re.sub(' +', ' ', mac_line)
                    split = mac_line_single_space.split()
                    mac = split[1]
                    return mac, Switch.convert_mac_addr(mac)
        return None, None


    @staticmethod
    def populate_mac_addrs(conn, port_dict):
        #print('total entries: {}'.format(len(port_dict)))
        for interface in port_dict.keys():
            if not Switch.check_interface(interface) or interface.count('15') < 1: # only get 15
                port_dict[interface]['mac_address_switch'] = None
                port_dict[interface]['mac_address_jamf'] = None
                continue
            command = Switch.COMMAND_MAC_ADDR.format(interface)
            Switch.debug('populate_mac_addrs: {}\n'.format(command), Switch.DEBUG_DEBUG)
            desc = conn.run(command, hide=True)
            new_desc = Switch.cut_disclaimer(desc.stdout)
            #print(type(new_desc))
            #print(new_desc, '\n')
            switch, jamf = Switch.parse_mac_addr_signature(new_desc)

            port_dict[interface]['mac_address_switch'] = switch
            port_dict[interface]['mac_address_jamf'] = jamf
            Switch.debug('populate_mac_addrs: port_dict["port15"]["mac_address_jamf"] -before- {}'.format(port_dict['port15']['mac_address_jamf']),Switch.DEBUG_DEBUG)
            if not port_dict['port15'].get('mac_address_switch'):
                port_dict['port15']['mac_address_switch'] = switch
                port_dict['port15']['mac_address_jamf'] = jamf
            Switch.debug('populate_mac_addrs: port_dict["port15"]["mac_address_jamf"] -after- {}'.format(port_dict['port15']['mac_address_jamf']),Switch.DEBUG_DEBUG)
            return port_dict

    @staticmethod
    def populate_violations(conn, port_dict):
        violations = Switch.count_violating_ports(conn)
        port_dict['violations'] = violations

    @staticmethod
    def run(conn, cmd):
        out = conn.run(cmd, hide=True).stdout
        clean_out = Switch.cut_disclaimer(out)
        return clean_out

    @staticmethod
    def run_report(user, pw, ip_addr, sap = None, switch = 'internal', 
                   DB_FILE = DB_FILE, TABLE = DB_TABLE,
                   ERR_TABLE = DB_ERR_TABLE):
        '''
            Reports on all devices connected to the switch
            @user: TMobile nt username
            @pw: TMobile nt password
            @sap: Sap to put status in Status table
            @switch: Can be internal, external, router, or signiture
            @DB_FILE: Sqlite db file to connect to
            @TABLE: Table to store switch data in
            @ERR_TABLE: Table to input errors
        '''
        old_ip = ip_addr
        ip_addr = Switch.swap_ip_full(ip_addr, switch)
        host = Switch.make_host_string(user, ip_addr)
        ck = {'password': pw}

        CONN = SQLite.create_sqlite(DB_FILE)
        ERR_COLUMNS = {'ip_address': 'text'}
        SQLite.create_table(CONN, ERR_TABLE, ERR_COLUMNS)
        ip_search = {'ip_address': old_ip}

        with Connection(host=host, connect_kwargs=ck) as conn:
            port_dict = {}
            port_dict['port15'] = {}
            try:
                port_dict = Switch.create_dict(conn) 
                Switch.debug('run_report: dictionary created', Switch.DEBUG_DEBUG)
                Switch.debug('run_report: dict {}'.format(port_dict), Switch.DEBUG_DEBUG)
                port_dict = Switch.populate_mac_addrs(conn, port_dict) 
                Switch.debug('run_report: mac addrs populated {}'.format(port_dict['port15'].get('mac_address_jamf')), Switch.DEBUG_INFO)
                Switch.populate_violations(conn, port_dict)
                Switch.debug('run_report: violations populated', Switch.DEBUG_DEBUG)

                # Cache results in sqlite database
                
                port15 = port_dict['port15']
                violations = port_dict.get('violations')
                
                port15.update({'violations': str(violations), 'ip_address': old_ip})
                new_columns = dict([(key, 'text') for key in port15])
                SQLite.create_table(CONN, TABLE, new_columns)
                SQLite.insert_or_update_rows(CONN, TABLE, columns=port15, where=ip_search)
                SQLite.delete_rows(CONN, ERR_TABLE, ip_search)

                rows = SQLite.select_rows(CONN, TABLE, where=ip_search)
                if len(rows): return dict(rows[0])
                else: raise Exception("Switch: No entry for ip {}".format(ip_addr))

            except Exception as e:
                Switch.debug(str(e), Switch.DEBUG_CRITICAL)
                SQLite.insert_or_update_rows(CONN, ERR_TABLE, ip_search, ip_search)

    @staticmethod
    def run_report_backup(ip_addr, switch='default', DB_FILE = 'TReports.db', TABLE = 'Port15_Statuses'):
        import packages.database.sqlite as sqlite
        CONN = SQLite.create_sqlite(DB_FILE)
        
        ip_addr = Switch.swap_ip(ip_addr, switch)
        try:
            rows = SQLite.select_rows(CONN, TABLE, where={'ip_address': ip_addr})
            if len(rows): return dict(rows[0])
            else: return None
        except sqlite.sqlite3.OperationalError as e:
            Switch.debug('Switch: '+str(e), Switch.DEBUG_CRITICAL)
            return None

    @staticmethod
    def swap_ip(ip, num):
        new_ip = ""
        octets = ip.split('.')
        for i in range(len(octets) - 1):
            new_ip += octets[i] + '.'
        new_ip += str(num)
        return new_ip

    @staticmethod
    def swap_ip_full(ip_addr, switch='internal'):
        octet_map = {
            'default': Switch.DEFAULT_OCTET,
            'internal': Switch.INTERNAL_OCTET,
            'external': Switch.EXTERNAL_OCTET,
            'router': Switch.ROUTER_OCTET,
            'signature': Switch.ROUTER_OCTET
        }
        if not switch or not octet_map.get(switch): switch = 'internal'
        ip_addr = Switch.swap_ip(ip_addr, octet_map[switch])
        return ip_addr

    @staticmethod
    def test_find_minis():
        # types = ['signature', 'cooporate', 'tpr', 
        #          'corporate', 'corporate', 'corporate']
        # saps = ['2916', '1538', '2046', '4235', '7886', '251F']
        ips = ['10.4.66.254', '10.120.25.6', '10.121.89.6', 
               '10.91.195.6', '10.47.88.6', '10.90.150.6']
        user = Switch.TEST_USERNAME
        pw = Switch.TEST_PASSWORD
        ck = {'password': pw}
        for ip_addr in ips:
            print()
            host = Switch.make_host_string(user, ip_addr)
            try:
                with Connection(host=host, connect_kwargs=ck) as conn:
                    minis = Switch.find_minis(conn)
                    print('{}'.format(ip_addr))
                    for mini in minis:
                        print()
                        for k, v in mini.items():
                            print('\t{}: {}'.format(k, v))
            except OSError as e:
                print(str(e)) 

    @staticmethod
    def test_find_mini_ports():
        Switch.DEBUG_LEVEL = Switch.DEBUG_INFO
        ips = ['10.4.66.254', '10.120.25.6', '10.121.89.6', 
               '10.91.195.6', '10.47.88.6', '10.90.150.6']
        user = Switch.TEST_USERNAME
        pw = Switch.TEST_PASSWORD
        ck = {'password': pw}
        for ip in ips:
            print()
            host = Switch.make_host_string(user, ip)
            try:
                with Connection(host=host, connect_kwargs=ck) as conn:
                    print('{}'.format(ip))
                    mini_ports = Switch.find_mini_ports(conn)
                    for port in mini_ports:
                        print()
                        for k, v in port.items():
                            print('\t{}: {}'.format(k, v))
            except OSError as e:
                print(str(e))

    @staticmethod
    def test_is_mini():
        mac_addrs = ['787b.8ab8.dad7', '08a5.c8a8.1343',
                     '24db.ad00.2281', 'a860.b63c.4f4c',
                     '9810.e8f0.2534', '10e7.c613.7383']
        for mac in mac_addrs:
            print('{}: {}'.format(mac, Switch.is_mini(mac)))

    @staticmethod
    def test_parse_mac_addr_signature():
        Switch.DEBUG_LEVEL = Switch.DEBUG_INFO
        ips = [('10.120.11.6', 'Gi1/0/15'), ('10.4.66.254', 'Gi5/46'), ('10.121.89.6','Gi2/0/4')]
        user = Switch.TEST_USERNAME
        pw = Switch.TEST_PASSWORD
        ck = {'password': pw}
        for ip, inf in ips:
            host = Switch.make_host_string(user, ip)
            with Connection(host=host, connect_kwargs=ck) as conn:
                cmd = Switch.COMMAND_MAC_ADDR.format(inf)
                try:
                    out = Switch.run(conn, cmd) # can fail and needs to retry / update errors
                    print(out)
                except:
                    Switch.debug('find_mac_addr: retrying', Switch.DEBUG_INFO)
                    out = Switch.run(conn, cmd)
                    print(out)
                switch_jamf = Switch.parse_mac_addr_signature(out)
                print(switch_jamf)

    @staticmethod
    def test_report():
        DB_FILE = Switch.DB_FILE
        TABLE = Switch.DB_TABLE
        #ERR_TABLE = TABLE + '_Errors' # Delete row if connect success
        #ERR_FILE = ERR_TABLE + '.csv'
        CONN = SQLite.create_sqlite(DB_FILE)
        #CSV_FILE =TABLE +  '.csv'
        ips = ['10.91.163.0', '10.91.247.0']
  
        #SQLite.drop_table(CONN, ERR_TABLE)
        #SQLite.drop_table(CONN, TABLE)
        print('Checking switches')
        for i in tqdm(range(len(ips))):
            ip = ips[i]
            report = Switch.run_report(Switch.TEST_USERNAME, Switch.TEST_PASSWORD, ip)
            if not report: continue
            for k, v in report.items():
                print('{}: {}'.format(k, v))

        #SQLite.sql_to_csv(CONN, CSV_FILE, TABLE)
        #SQLite.sql_to_csv(CONN, ERR_FILE, ERR_TABLE)
        rows = SQLite.select_rows(CONN, TABLE, columns={}, where={})
        for row in rows:
            entry = dict(row)
            for k,v in entry.items():
                print('{}: {}'.format(k, v))

    @staticmethod
    def test_report_backup():
        ips = ['10.30.46.6', '10.47.54.6', '10.91.163.6', '10.91.247.6']
        for ip in ips:
            report = Switch.run_report_backup(ip)
            if not report: continue
            for k, v in report.items():
                print('{}: {}'.format(k, v))


if __name__ == '__main__':
    #Switch.test_parse_mac_addr_signature()
    Switch.test_find_mini_ports()

    # TODO: Create this function
        # Will get switch report for a set of ip addresses
        # Could be fun to store them in a database
        # Could count errors
            # Correct errors
            # Send to CSV
            # Send to JSON - update sql package