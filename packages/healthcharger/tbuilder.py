import json

from bs4 import BeautifulSoup
from tqdm import tqdm 
import requests

from packages.utility.utility import Utility as UTIL
from packages.database.sqlite import SQLite
from packages.healthcharger.status import Status

class TBuilder:
    DB_TABLE = 'TReports'
    DB_FILE = 'TReports.db'
    DB_CSV_FILE = 'TReports.csv'

    TEST_USERNAME = ''
    TEST_PASSWORD = ''

    DEBUG_LEVEL = 0
    DEBUG_CRITICAL = 1
    DEBUG_MAJOR = 2
    DEBUG_INFO = 3
    DEBUG_DEBUG = 4

    HOME_URL = 'https://builder.rreis.com/home'
    AUTH_URL = 'https://builder.rreis.com/login/auth'
    POST_AUTH_URL = 'https://builder.rreis.com/j_spring_security_check'
    PAGE_ONE_URL = 'https://builder.rreis.com/workflow/search?q={}&field=SAP&offset={}' # .format(unpadded_sap, results_offset)
    PAGE_TWO_URL = ' https://builder.rreis.com/workflow/store/{}/form/51' # ,format(site_id)
    
    HEADERS = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9,pl;q=0.8',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Content-Length': '45',
        'Content-Type': 'application/x-www-form-urlencoded',

        'Host': 'builder.rreis.com',
     #   'Origin': 'https://builder.rreis.com',
     #   'Referer': 'https://builder.rreis.com/login/auth',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    }
    
    LOGIN_DATA = {
        'j_username': '',
        'j_password': ''
    }

    DEBUG_LEVEL = 0

    @staticmethod
    def clean_report(report, filter_cols):
        '''
            Filters report down to filter_cols
            @report: t_report
            @filter_cols: columns we want to keep
        '''
        report.update([(key, None) for key in filter_cols if key not in report])
        bad_cols = [col for col in report.keys() if col not in filter_cols]
        for col in bad_cols:
            del report[col]
        return report

    @staticmethod
    def debug(msg, lvl, funcname = 'TBuilder'):
        if lvl <= TBuilder.DEBUG_LEVEL:
            print('{}:{}'.format(msg, funcname))

    @staticmethod
    def error_log(sap, login_failure = False):
        '''
            Log that a sap was not found in StatusTable
            @sap: Sap to log
            @login_failure: Whether failure due to login or something else
        '''
        status = Status.TBUILDER_SAP_NOT_FOUND
        if login_failure: status = Status.TBUILDER_LOGIN_FAILURE
        Status.insert_or_update(sap=sap, status=status)
    
    @staticmethod
    def login(ntid, pw):
        # Returns sess or None
        # Could be useful in test setups
        sess = requests.Session()
        url = TBuilder.HOME_URL
        auth_url = TBuilder.POST_AUTH_URL
        headers = TBuilder.HEADERS
        login_data = TBuilder.LOGIN_DATA
        login_data['j_username'] = ntid
        login_data['j_password'] = pw
        TBuilder.debug('TBuilder: found login page', 2)

        sess.get(url, timeout=5, allow_redirects=True) # Took headers out and it stopped hanging

        TBuilder.debug('TBuilder: got tbuilder token {}'.format(sess.cookies.get_dict()), 2)
        headers['Cookie'] = json.dumps(sess.cookies.get_dict()) # may be useless

        sess.post(auth_url, data=login_data, headers=headers, timeout=5) 

        # TODO: Put error handingling here, for lack of access
        a = sess.get(TBuilder.HOME_URL, headers=sess.headers, timeout=5)
        ln = len(a.text)
        success = ln != 5813
        TBuilder.debug('TBuilder: login test -> 5818 != {} -> {}'.format(ln, success), 2)
        if success:
            return sess
        else: 
            return None
     
    @staticmethod
    def get_site_id(sess, sap):
        # Expects sess to be logged in
        # Returns dictionary of properties for that sap
        # Can unit test by passing various saps
        TBuilder.debug('TBuilder: get_site_id start', 2)
        text = ''
        try:
            offset = 0
            entry = {}
            while(True): # Loop until one of two cases
                url = TBuilder.PAGE_ONE_URL.format(sap, offset)
                res = sess.get(url, headers=sess.headers, timeout=5)
                text = res.text
                soup = BeautifulSoup(text, 'html.parser')
                trs = soup.find_all('tr')
                if len(trs) < 2:            # Base Case : Not found in TBuilder
                    TBuilder.debug('TBuilder: {} not found'.format(sap), 3)
                    TBuilder.debug('TBuilder: tr count - {}'.format(len(trs)), 3)
                    return None 

                TBuilder.debug('trs: {}\n'.format(len(trs)),3)
                for i in range(len(trs)):   # each entry with unique site id
                    tr = trs[i]
                    TBuilder.debug('tr {}'.format(i),3)
                    
                    tds = tr.find_all('td')
                    TBuilder.debug('\ttds: {}'.format(len(tds)),3)
                    open_, match_sap, entry = False, False, {}
                    for td in tds:          # each property of each entry
                        samp = td.find('samp')
                        if samp:
                            samp_name = samp.get('name')
                            samp_text = samp.string
                            TBuilder.debug('\t {} - {}'.format(samp_name, samp_text),3)
                            TBuilder.debug('',3)
                            entry[samp_name] = samp_text
                            if samp_text:
                                if samp_text.lower() == sap.lower():
                                    match_sap = True
                                if samp_text.lower() == 'open' or samp_text.lower() == 'future launch':
                                    open_ = True
                                if open_ and samp_text.lower() == sap.lower():
                                    TBuilder.debug('match_sap {} - {}'.format(sap, 'Open'),3)
                                
                    if match_sap and open_:
                        TBuilder.debug(entry, 2)
                        return entry        # return all properties of desired entry
                offset += 10
                TBuilder.debug('TBuilder Query 1: offset increase - {}'.format(offset), 2)
            #print(soup.prettify())
            
        except Exception as e:
            print(str(e))
            return None
        # 1. Soup(text)
        # 2. Soup-based Parse Algorithm 
        # 3. Return
    
    @staticmethod
    def get_form_51(sess, site_id):
        # Expects sess to be logged in
        # Returns data object 
        # Can unit test by passing various site_ids
        TBuilder.debug('TBuilder: get_form_51 start', 2)
        try:
            url = TBuilder.PAGE_TWO_URL.format(site_id)
            res = sess.get(url, headers=sess.headers, timeout=5)
            text = res.text
            soup = BeautifulSoup(text, 'html.parser')
            samps = soup.find_all('samp')
            data_obj = {}
            for i in range(len(samps)):
                samp = samps[i]
                key = samp.get('name')
                value = samp.string
                TBuilder.debug('TBuilder: \t{} - {}'.format(key,value),2)
                if key and not data_obj.get(key):
                    data_obj[key] = value
                elif not key and value and (value.lower() == 'yes' or value.lower() == 'no'):
                    data_obj['site.on_network'] = value
            return data_obj
            #print(soup.prettify())

        except Exception as e:
            print(str(e))
            return None
        
    @staticmethod
    def report(ntid=None, pw=None, saps=[],
               DB_FILE = 'TReports.db', DB_TABLE = 'TReports'):
        '''
            Fetches info for a sap from TBuilder
            @ntid: TBuilder login username [str]
            @pw: TBuilder login password [str]
            @saps: Saps to fetch info for. [list]
            @DB_FILE: 'sqlite db file to connect to'
            @DB_TABLE: 'sqlite table to store the info in'
        '''
        sess = TBuilder.login(ntid, pw)
        if not sess: 
            for sap in saps:
                TBuilder.error_log(sap, login_failure=True)
            return []
        t_reports = []
        for i in range(len(saps)):
            sap = saps[i]
            open_store, form_data = None, None
            open_store = TBuilder.get_site_id(sess, sap)
            if open_store:
                form_data = TBuilder.get_form_51(sess, open_store.get('site.id'))
                if form_data:
                    for key in open_store.keys(): form_data[key] = open_store[key]
                    t_reports.append(form_data)
                else: TBuilder.error_log(sap)
            else: TBuilder.error_log(sap)
        sess.close()

        if not len(t_reports): return []

        # Add to database - need to test
        CONN = SQLite.create_sqlite(DB_FILE)
        report = t_reports[0]
        report = SQLite.sterilize_keys(report)
        sap = report.get('site_sap')
        sap_search = {'site_sap': sap}
        columns = dict([(key, 'text') for key in report.keys() if key])
        SQLite.create_table(CONN, DB_TABLE, columns)
        rows = SQLite.select_rows(CONN, DB_TABLE)
        if not len(rows):
            SQLite.insert_or_update_rows(CONN, DB_TABLE, report, sap_search)
            rows = SQLite.select_rows(CONN, DB_TABLE)

        if len(rows): template = dict(rows[0])
        else: template = columns
        filter_cols = template.keys()

        clean_reports = []
        for report in t_reports:
            report = SQLite.sterilize_keys(report)
            new_report = TBuilder.clean_report(report, filter_cols)
            ip = new_report.get('site_default_gw')
            if ip and ip.count('and'):
                ip_list = ip.split()
                new_report['site_default_gw'] = ip_list[0]
            sap = new_report.get('site_sap')
            sap_search = {'site_sap': sap}
            SQLite.insert_or_update_rows(CONN, DB_TABLE, new_report, sap_search)
            rows = SQLite.select_rows(CONN, DB_TABLE, where=sap_search)
            clean_reports.append(dict(rows[0]))

        return clean_reports


    @staticmethod
    def report_backup(DB_FILE='TReports.db', DB_TABLE='TReports', saps=[]):
        '''
            Checks for data saved in database
                - If not found, checks for data saved in csv file
                    - If not found, return None
            Returns dictionaries of data from specified saps
        '''
        import packages.database.sqlite as sqlite
        sap_dicts = []
        CONN = SQLite.create_sqlite(DB_FILE)
        try:
            for i in tqdm(range(len(saps))):
                sap = saps[i]
                rows = SQLite.select_rows(CONN, DB_TABLE, where={'site.sap': sap})
                if len(rows): sap_dicts.append(dict(rows[0]))
        except sqlite.sqlite3.OperationalError as e:
            TBuilder.debug('TBuilder: '+str(e), TBuilder.DEBUG_CRITICAL)
            return []
        return sap_dicts

    @staticmethod
    def test_report():
        '''
            Tests the report method
        '''
        #sap_csv = 'mac_stations_port_15_status_clean_up.csv'
        #tickets = UTIL.read_csv(sap_csv)
        #saps = [ticket.get('SAP') for ticket in tickets]
        #IP_FILE = 'ip_addresses.csv'
        saps = ['985E', '976E','955D']
        reports = TBuilder.report(TBuilder.TEST_USERNAME, TBuilder.TEST_PASSWORD, saps)
        for report in reports:
            sap = report.get('site_sap')
            ip = report.get('site_default_gw')
            print('{}: {}'.format(sap, ip))

    @staticmethod
    def test_report_backup():
        '''
            Tests the report backup method
        '''
        saps = ['985E', '976E','955D']
        reports = TBuilder.report_backup(saps=saps)
        for report in reports:
            sap = report.get('site_sap')
            ip = report.get('site_default_gw')
            print('{}: {}'.format(sap, ip))

    @staticmethod
    def test_signature_store():
        '''
            Test ability to scrape signature store knowledge
        '''
        saps = ['2916']
        reports = TBuilder.report(TBuilder.TEST_USERNAME, TBuilder.TEST_PASSWORD, saps)
        if len(reports):
            report = reports[0]
            print('ip: {}'.format(report.get('site_default_gw')))
            print('type: {}'.format(report.get('site_design_code')))

    @staticmethod
    def test_sap_not_found():
        '''
            Tests ability to handle sap not found
        '''
        saps = ['5386']
        reports = TBuilder.report(TBuilder.TEST_USERNAME, TBuilder.TEST_PASSWORD, saps)
        if len(reports):
            report = reports[0]
            print('ip: {}'.format(report.get('site_default_gw')))
            print('type: {}'.format(report.get('site_design_code')))
        else: 
            print('Future Launch not working')


if __name__ == '__main__':
    Status.provision()
    TBuilder.test_sap_not_found()