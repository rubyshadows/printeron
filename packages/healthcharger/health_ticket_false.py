'''
This module is just a temporary substitute for the real Health Ticket class.
This class was copied from the old health charger ticket class for its make ticket method.
'''
from inspect import currentframe as cf

from tqdm import tqdm

from packages.pier.pier import Pier
from packages.healthcharger.health_utility import HealthUtility

# cf().f_code.co_name
                
class Ticket:
    '''
        Class to represent a ticket
    '''
    test_id = '23594005'
    nt_user = "NTMServiceUser"
    def __init__(self, ticket_id=None, assignee={"UserCode": None, "UserName": None}, 
                 cause={"CauseDescription": None},
                 desc=None, element_id=None, group=None, status=None, requester={"UserCode": None, "UserName": None},
                 sap=None):
        '''
            These properties are taken from the Pier JSON ticket objects
        '''
        self.ticket_id = ticket_id                      # TicketId
        self.assignee = assignee.get('UserCode')        # Assignee: UserCode / NTID
        self.assignee_name = assignee.get('UserName')   # Assignee: UserNAme
        self.cause = cause                              # CauseDescription
        self.element_id = element_id                    # ElementID
        self.desc = desc                                # Description
        self.group = group                              # AssigneeGroup
        self.status = status                            # Status
        self.requester = requester                      # RequestedBy : {UserCode:SAP, UserName: ""}
        self.sap = sap
        
    @staticmethod
    def make_tickets(ticket_dicts):
        '''
            Turns ticket JSON objects into Ticket objects 
            @ticket_dicts: the TicketDetail JSON object from a Pier query
        '''
        tickets = []
        #print("Pier.make_tickets: ticket_dicts length: {}".format(len(ticket_dicts)))
        if not ticket_dicts or len(ticket_dicts) == 0:
            print('No tickets')
            return []
        print('Ticket.make_tickets: Creating ticket objects')
        for i in tqdm(range(len(ticket_dicts))):
            t = ticket_dicts[i]
            if t.get('TicketDetail'):
                ticket = t.get('TicketDetail')
            else:
                print('no ticketdetail for ticket number {} in the list'.format(i))
                ticket = t
            if not ticket.get("TicketId"): continue
            ticket_id = ticket["TicketId"]
            requester = ticket.get("RequestedBy") #{UserCode: SAP, UserName:""}
            desc = ticket["Description"]
            worklogs = Ticket.get_worklog(ticket_id)
            sap = requester
            HealthUtility.debug(cf().f_code.co_name, 'sap from requester - {}'.format(sap), 1)
            if not HealthUtility.check_sap_form(sap):
                sap = HealthUtility.sap_from_description(desc)
                HealthUtility.debug(cf().f_code.co_name, 'sap from description - {}'.format(sap), 1)
                if not HealthUtility.check_sap_form(sap):
                    sap = HealthUtility.sap_from_worklog(worklogs)
                    HealthUtility.debug(cf().f_code.co_name, 'sap from worklog - {}'.format(sap), 1)
                    if not HealthUtility.check_sap_form(sap):
                        sap = None
            #sap = HealthUtility.sap_from_worklog(worklogs)
            if ticket.get("ElementId"):
                element_id = ticket["ElementId"]
            else:
                element_id = ticket.get('Element').get('ElementId')
            assignee = ticket["Assignee"] # {UserCode, UserName}
            cause = ticket.get('CauseDescription') or ticket.get('Cause').get('Description')
            status = ticket.get("StatusDescription") or ticket.get("Status")
            group = ticket.get("GroupName") or ticket.get("UserGroup").get("Description")
            
            tickets.append(Ticket(ticket_id, assignee, cause, desc, element_id, group, status, requester, sap))
        tickets.sort(key=lambda x: x.ticket_id, reverse = True) # sorted by creation date
        return tickets
    
    @staticmethod
    def get_worklog(ticket_id):
        res = Pier.get_request("worklogs", [ticket_id])
        return res['WorkLogs']
        
    @staticmethod
    def update_worklog(ntid, ticket_id, text):
        data = {
            "UserId":ntid,
            "TicketId":ticket_id,
            "UserFullName":"", #testing
            "WorkLogs":[{
                "LogText":text, #testing
                "CreatedBy":"", #testing
                "Auto_Created": "true",
            }]
        }
        try:
            Pier.post_request("add_worklog", [], data)
        except Exception as e:
            print(str(e))
            print('Ticket: Trouble updating ticket {}'.format(ticket_id))
        
    @staticmethod
    def update_testlog(ntid, text):
        data = {
            "UserId":ntid,
            "TicketId":Ticket.test_id,
            "UserFullName":"", #testing
            "WorkLogs":[{
                "LogText":text, #testing
                "CreatedBy":"", #testing
                "Auto_Created": "true",
            }]
        }
        try:
            Pier.post_request("add_worklog", [], data)
        except Exception as e:
            print(str(e))
            print('Ticket: Test ticket {} may have been terminated.'.format(Ticket.test_id))