import unittest
from packages.database.sqlite import SQLite

class TestSQLite(unittest.TestCase):

    def setUp(self):
        self.table =                'Test'
        self.species_table =        'SpeciesTable'
        self.poke_table =           'PokeTable'
        self.columns =              {'name':'TEXT UNIQUE NOT NULL','age':'INTEGER','date':'TEXT'}
        self.species_columns =      {'species': 'text unique not null', 'type': 'text'}
        self.poke_columns =         {'species': 'text', 'name': 'text', 'trainer': 'text'}
        self.select_columns =       {'name', 'age', 'date'}
        self.columns_misty =        {'name': 'Misty', 'age': 18, 'date': '1994-03-21'}
        self.columns_brock =        {'name': 'Brock', 'age': 21, 'date': '1991-10-18'}
        self.columns_clair =        {'name': 'Clair', 'age': 33, 'date': '1979-12-1'}
        self.columns_aldred =       {'name': 'Aldred', 'age': 40, 'date': '1972-2-9'}
        self.columns_melino =       {'name': 'Melino', 'age': None, 'date': None}

        self.columns_pidgey =       {'species': 'Pidgey', 'type': 'Flying'}
        self.columns_snorelax =     {'species': 'Snorlax', 'type': 'Normal'}
        self.columns_bulbasaur =    {'species': 'Bulbasaur', 'type': 'Grass'}
        self.columns_charmander =   {'species': 'Charmander', 'type': 'Fire'}
        self.columns_squirtle =     {'species': 'Squirtle', 'type': 'Water'}
        self.columns_omanyte =      {'species': 'Omanyte', 'type': 'Rock'}
        self.columns_dragonite =    {'species': 'Dragonite', 'type': 'Dragon'}
        self.columns_staryu =       {'species': 'Staryu', 'type': 'Water'}

        self.hitodeman =            {'species':'Staryu', 'name': 'Hitodeman', 'trainer': 'Misty'}
        self.dragonite =            {'species': 'Dragonite', 'name': None, 'trainer': 'Clair'}

        self.where_misty =          {'name': 'Misty'}
        self.where_brock =          {'name': 'Brock'}
        self.where_clair =          {'name': 'Clair'}
        self.where_aldred =         {'name': 'Aldred'}
        self.where_melino =         {'name': 'Melino'}
        self.conn =             SQLite.create_sqlite()
        SQLite.create_table(self.conn, self.table, self.columns)
        SQLite.create_table(self.conn, self.species_table, self.species_columns)
        SQLite.create_table(self.conn, self.poke_table, self.poke_columns)

    def tearDown(self):
        SQLite.drop_table(self.conn, self.table)

    def test_1_read_empty(self):
        columns = {'name': None}
        res = SQLite.select_rows(self.conn, self.table, columns, self.where_brock)
        self.assertEqual(len(res), 0)

    def test_2_insert_row(self):
        SQLite.insert_row(self.conn, self.table, self.columns_misty)
        res = SQLite.select_rows(self.conn, self.table, self.select_columns)
        self.assertEqual(len(res), 1, 'Length of result not 1 row')
        self.assertEqual('Misty' in res[0], True, str(res[0]))

        SQLite.insert_row(self.conn, self.table, self.columns_brock)
        res = SQLite.select_rows(self.conn, self.table, self.select_columns)
        self.assertEqual(len(res), 2)
        self.assertEqual(True, 'Brock' in res[0] or 'Brock' in res[1])
        self.assertEqual(True, 'Misty' in res[0] or 'Misty' in res[1])

    def test_3_read_rows(self):
        select_columns = {'name': None}
        SQLite.insert_row(self.conn, self.table, self.columns_misty)
        res = SQLite.select_rows(self.conn, self.table, select_columns, self.where_brock)
        self.assertEqual(len(res), 0)

        SQLite.insert_row(self.conn, self.table, self.columns_brock)
        res = SQLite.select_rows(self.conn, self.table, select_columns, self.where_brock)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][0], 'Brock')

    def test_read_null(self):
        select_columns = {'name'}
        SQLite.insert_row(self.conn, self.table, self.columns_misty)
        SQLite.insert_row(self.conn, self.table, self.columns_melino)
        res = SQLite.select_rows(self.conn, self.table, select_columns, where = {'age': None})
        self.assertEqual(len(res), 1)

    def test_delete_rows_empty(self):
        select_columns = {'age'}
        SQLite.delete_rows(self.conn, self.table, self.where_misty)
        ages = SQLite.select_rows(self.conn, self.table, select_columns, self.where_misty)
        self.assertEqual(len(ages), 0)

    def test_delete_rows_whereless(self):
        SQLite.insert_row(self.conn, self.table, self.columns_clair)
        SQLite.insert_row(self.conn, self.table, self.columns_aldred)
        SQLite.delete_rows(self.conn, self.table)
        rows = SQLite.select_rows(self.conn, self.table)
        self.assertEqual(len(rows), 0)

    def test_delete_rows(self):
        SQLite.insert_row(self.conn, self.table, self.columns_clair)
        SQLite.insert_row(self.conn, self.table, self.columns_aldred)
        SQLite.delete_rows(self.conn, self.table, self.where_aldred)
        rows = SQLite.select_rows(self.conn, self.table)
        self.assertEqual(len(rows), 1)
        self.assertEqual('Clair' in rows[0], True)

    def test_join_tables(self):
        SQLite.insert_row(self.conn, self.table, self.columns_clair)
        SQLite.insert_row(self.conn, self.table, self.columns_misty)
        SQLite.insert_row(self.conn, self.species_table, self.columns_dragonite)
        SQLite.insert_row(self.conn, self.species_table, self.columns_staryu)
        SQLite.insert_row(self.conn, self.poke_table, self.hitodeman)
        SQLite.insert_row(self.conn, self.poke_table, self.dragonite)
        trainer_poke_condition = '{}$name = {}$trainer'.format(self.table, self.poke_table)
        species_poke_condition = '{}$species = {}$species'.format(self.species_table, self.poke_table)
        # select_columns = ['{}$name as nickname'.format(self.poke_table)]
        join1 = {'join_type': 'inner', 'table': self.table, 'condition': trainer_poke_condition}
        join2 = {'join_type': 'inner', 'table': self.species_table, 'condition': species_poke_condition}
        rows = SQLite.select_rows(self.conn, self.poke_table, columns = {}, joins = [join1, join2])
        self.assertEqual(len(rows), 2)
        results = [dict(row) for row in rows]
        # print(results[0].keys()) - TODO: Debug functions for test suite
        print()
        for result in results:
            for k, v in result.items():
                print('{}: {}'.format(k, v))
            print()

    def test_update_rows_empty(self):
        update_columns = {'age': 19}
        select_columns = {'age'}
        SQLite.update_rows(self.conn, self.table, update_columns, self.where_misty)
        ages = SQLite.select_rows(self.conn, self.table, select_columns, self.where_misty)
        self.assertEqual(len(ages), 0)

    def test_update_null(self):
        update_columns = {'age': 19, 'date': '1970-1-1'}
        select_columns = ['name', 'age', 'date']
        SQLite.insert_row(self.conn, self.table, self.columns_clair)
        SQLite.insert_row(self.conn, self.table, self.columns_aldred)
        SQLite.insert_row(self.conn, self.table, self.columns_melino)
        SQLite.update_rows(self.conn, self.table, update_columns, {'age': None})
        rows = SQLite.select_rows(self.conn, self.table, select_columns, self.where_melino)
        mel = dict(rows[0])
        self.assertEqual(mel.get('age'), 19)
        self.assertEqual(mel.get('date'), '1970-1-1')

    def test_update_rows_whereless(self):
        update_columns = {'age': 19}
        select_columns = {'age'}
        SQLite.insert_row(self.conn, self.table, self.columns_clair)
        SQLite.insert_row(self.conn, self.table, self.columns_aldred)
        SQLite.update_rows(self.conn, self.table, update_columns)
        ages = SQLite.select_rows(self.conn, self.table, select_columns, {'age': 19})
        self.assertEqual(len(ages), 2)

    def test_update_rows(self):
        update_columns = {'age': 19}
        select_columns = {'name', 'age'}
        SQLite.insert_row(self.conn, self.table, self.columns_clair)
        SQLite.insert_row(self.conn, self.table, self.columns_aldred)
        SQLite.update_rows(self.conn, self.table, update_columns, {'name': 'Clair'})
        res = SQLite.select_rows(self.conn, self.table, select_columns, {'age': 19})
        self.assertEqual(len(res), 1)
        self.assertEqual(True, 'Clair' in res[0])