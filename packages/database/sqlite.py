import sqlite3

'''
TODO:
    - Create generic Select query creator function
    - Create generic Insert query creator function
    - Create other generic query creator functions
'''

class JoinException(Exception):
    pass

class SQLite:

    DEBUG_LEVEL = 0
    DEBUG_CRITICAL = 1
    DEBUG_MAJOR = 2
    DEBUG_INFO = 3
    DEBUG_DEBUG = 4

    @staticmethod
    def create_sqlite(filename = ':memory:'):
        '''
            Create sqlite connection object
            @filename: nane of db file. Defaults to in memory database
        '''
        conn = sqlite3.connect(filename)
        conn.row_factory = sqlite3.Row
        return conn

    @staticmethod
    def create_table(conn, table, columns):
        '''
            Create Table
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of the table [str]
            @columns: column name : type pairs [dict]
        '''
        columns = SQLite.sterilize_keys(columns)
        with conn:
            c = conn.cursor()
            query = """CREATE TABLE IF NOT EXISTS {} ({})"""
            append = ''
            delim = ''
            for key, val in columns.items():

                append += delim + ' '
                append += key + ' ' + val
                delim = ','
            query = query.format(table, append)
            SQLite.debug(query, SQLite.DEBUG_INFO)
            c.execute(query)

    @staticmethod
    def delete_rows(conn, table, where = {}):
        '''
            Delete rows from table
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of table [str]
            @where: column:value pairs [dict] - filter rows to delete
        '''
        where = SQLite.sterilize_keys(where)
        with conn:
            c = conn.cursor()
            delete_clause = SQLite.make_delete_clause()
            from_clause = SQLite.make_from_clause(table)
            where_clause = SQLite.make_where_clause(where)
            query = '{} {} {}'.format(delete_clause, from_clause, where_clause)
            SQLite.debug(query, SQLite.DEBUG_INFO)
            c.execute(query, where)
            
    @staticmethod
    def drop_table(conn, table):
        '''
            Delete Table
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of the table [str]
        '''
        with conn:
            c = conn.cursor()
            query = 'DROP TABLE IF EXISTS {}'.format(table)
            SQLite.debug(query, SQLite.DEBUG_INFO)
            c.execute(query)

    @staticmethod
    def insert_or_update_rows(conn, table, columns = {}, where = {}):
        '''
            Inserts row if not exists, updates otherwise
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of the table [str]
            @columns: column name : value pairs [dict]
            @where: search criteria
        '''
        rows = SQLite.select_rows(conn, table, columns, where)
        if not len(rows): SQLite.insert_row(conn, table, columns)
        else:
            SQLite.update_rows(conn, table, columns, where)

    @staticmethod
    def insert_row(conn, table, columns):
        '''
            Creates row in table
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of the table [str]
            @columns: column name : value pairs [dict]
        '''
        columns = SQLite.sterilize_keys(columns)
        with conn:
            c = conn.cursor()
            query = SQLite.make_insert_clause(table, columns)
            SQLite.debug(query, SQLite.DEBUG_INFO)
            c.execute(query, columns)

    @staticmethod
    def select_rows(conn, table, columns = {}, where = {}, joins = []):
        '''
            Read rows from table
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of the table [str]
            @columns: columns to return [dict]
            @where: columnname:value pair [dict]
            @joins: [{join_type:str, table:str, condition:str}]
            Returns: list of result dictionaries [list of dict]
        '''
        columns = SQLite.sterilize_keys(columns)
        where = SQLite.sterilize_keys(where)
        with conn:
            c = conn.cursor()
            select_clause = SQLite.make_select_clause(columns)
            from_clause = SQLite.make_from_clause(table)
            where_clause = SQLite.make_where_clause(where)
            join_clause = SQLite.make_join_clause(joins)
            query = '{} {} {} {}'.format(select_clause, from_clause, join_clause, where_clause)
            SQLite.debug(query, SQLite.DEBUG_INFO)
            c.execute(query, where)
            return c.fetchall()

    @staticmethod
    def update_rows(conn, table, columns, where = {}):
        '''
            Update rows of the table
            @conn: sqlite connection object [sqlite3.connect(filename)]
            @table: name of table [str]
            @columns: column:value pairs [dict] - new value used to update
            @where: column:value pairs [dict] - filter rows to update
            Notes: Updates all columns if no where clause specified
        '''
        columns = SQLite.sterilize_keys(columns)
        where = SQLite.sterilize_keys(where)
        with conn:
            c = conn.cursor()
            update_clause = SQLite.make_update_clause(table, columns)
            where_clause = SQLite.make_where_clause(where)
            query = '{} {}'.format(update_clause, where_clause)
            SQLite.debug(query, SQLite.DEBUG_INFO)
            combined = SQLite.combine_dicts(where, columns)
            c.execute(query, combined)

################### Query Builders #################
    @staticmethod
    def combine_dicts(dict1, dict2):
        '''
            Combines two dictionaries
            Note: dict2 values will overwrite dict1 values with same key
        '''
        new_dict = {}
        new_dict.update(dict1)
        new_dict.update(dict2)
        return new_dict

    @staticmethod
    def make_delete_clause():
        '''
            Creates DELETE clause
        '''
        return 'DELETE'

    @staticmethod
    def make_from_clause(clause):
        '''
            Creates FROM clause
        '''
        query = 'FROM {}'.format(clause)
        return query

    @staticmethod
    def make_insert_clause(table, columns):
        '''
            Creates INSERT INTO clause
        '''
        keys = '('
        delim = ''
        for key in columns.keys():
            keys += delim
            keys += ':{}'.format(key)
            delim = ', '
        keys += ')'
        query = """INSERT INTO {} VALUES {}""".format(table, keys)
        return query

    @staticmethod
    def make_join_clause(table_joins):
        '''
            Makes JOIN clause
            @table_joins: [{join_type:str, table:str, condition:str}]
        '''
        if not table_joins or not len(table_joins): return ''
        join_types = {'LEFT', 'RIGHT', 'INNER', 'CROSS', 'NATURAL'}
        query = ''
        for join in table_joins:
            join_type = join.get('join_type')
            table = join.get('table')
            condition = SQLite.sterilize_string(join.get('condition'))
            if not join_type or not table or not condition:
                raise JoinException("sqlite: join_type, table, and condition needed")
            join_type = join_type.upper()
            if join_type not in join_types:
                raise JoinException("sqlite: {} is not a supported join".format(join_type))
            query += join_type + ' JOIN '+ table + ' ON ' + condition + '\n'
        return query

    @staticmethod
    def make_select_clause(columns):
        '''
            Creates SELECT clause for read queries
            @columns: columns to return
        '''
        select_keys = ''
        delim = ''
        for key in columns:
            select_keys += delim
            select_keys += key
            delim = ', '
        if not select_keys:
            select_keys = '*'
        query = 'SELECT {}'.format(select_keys)
        return query

    @staticmethod
    def make_update_clause(table, columns):
        '''
            Creates UPDATE clause for update query
            @columns: column:value pairs
        '''
        set_keys = ''
        delim = ''
        for key in columns.keys():
            set_keys += delim
            set_keys += '{} = :{}'.format(key, key)
            delim = ', '
        query = 'UPDATE {} SET {}'.format(table, set_keys)
        return query

    @staticmethod
    def make_where_clause(columns):
        '''
            Creates WHERE clause for most queries
            @columns: column:value pairs
        '''
        where_keys = ''
        delim = ''
        for key, value in columns.items():
            where_keys += delim
            if value:
                where_keys += '{}=:{}'.format(key, key)
            else:
                where_keys += '{} IS NULL'.format(key)
            delim = ' AND '
        if not where_keys:
            query = ''
        else:
            query = 'WHERE {}'.format(where_keys)
        return query

    @staticmethod
    def sterilize_keys(collection):
        '''
            Replaces illegal characters with _
        '''
        if not len(collection): return collection
        if isinstance(collection, dict):
            new_dict = {}
            for key,value in collection.items():
                if not key: continue
                new_key = SQLite.sterilize_string(key)
                new_dict[new_key] = value
            return new_dict
        elif isinstance(collection, list):
            new_list = []
            for key in collection:
                new_list.append(SQLite.sterilize_string(key))
            return new_list
        elif isinstance(collection, set):
            new_set = set()
            for key in collection:
                new_set.add(SQLite.sterilize_string(key))
            return new_set
        else:
            return collection

    @staticmethod
    def sterilize_string(string):
        '''
            Replaces illegal special characters
        '''
        return string.replace('.', '_').replace('$', '.')

#############################################
##### Functions requiring dependencies ######
#############################################

    @staticmethod
    def sql_to_csv(conn, filename, table, columns = {}, where = {}):
        '''
            Sends results from select query to csv file
        '''
        from packages.utility.utility import Utility as util
        with conn:
            rows = SQLite.select_rows(conn, table, columns, where)
            if not len(rows): return
            new_rows = []
            for row in rows:
                new_rows.append(dict(row))
            util.write_csv(filename, new_rows)

############################
##### Debug Functions ######
############################

    @staticmethod
    def debug(msg, lvl):
        if lvl <= SQLite.DEBUG_LEVEL:
            print(msg)