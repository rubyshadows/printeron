from inspect import currentframe as cf

import openpyxl
from openpyxl.utils.exceptions import InvalidFileException

class Excel:

    @staticmethod
    def error_message(func_name, message, error_class):
        msg = '{}: {}'.format(func_name, message)
        raise error_class(msg)

    @staticmethod
    def excel_open_book(book_name):
        '''
            @book_name: The book_name of the excel workbook to read
            Returns: An excel book object
        '''
        try:
            book = openpyxl.load_workbook(book_name)
        except FileNotFoundError:
            print('File not found {}'.format(book_name))
            return None
        except PermissionError:
            print('File {} is open. Please close file.'.format(book_name))
            return None
        except InvalidFileException:
            print('Please use a .xlsx file extension for file {}.'.format(book_name))
            return None
        return book

    @staticmethod
    def excel_get_header_indicies(book, sheet, headers):
        '''
            https://stackoverflow.com/questions/51975912/get-column-names-of-excel-worksheet-with-openpyxl-in-readonly-mode
            @book: The excel book object to read from
            @fields: The fieldnames to extract indicies from
            Returns: A dictionary of {field: idx} pairs
        '''
        result = {}
        ordered_headers = []
        sheet = book[sheet]
        header_row = sheet[1]
        for cell in header_row:
            ordered_headers.append(cell.value)
        print("ordered_headers")
        print(ordered_headers)
        for i in range(len(ordered_headers)):
            header = ordered_headers[i]
            if header in headers:
                result[i+1] = header
        return result

    @staticmethod
    def excel_read_book(book, sheet, fields):
        '''
            @book: The excel book object to read from
            @fields: The fieldnames to extract rows from
            Returns: A dictionary of indicies, containing dictionaries key'd by fields
            Note: Excel file must have header row
        '''
        # identify the column numbers of interest, based on field names
        HEADER_ROW = 2
        res = {}
        indicies = Excel.excel_get_header_indicies(book, sheet, fields)
        wc = book[sheet]
        for row_cells in wc.iter_rows(min_row=HEADER_ROW):
            for cell in row_cells:
                # only capture the cells of interest
                if cell.column in indicies:
                    obj = {'index': cell.row, indicies[cell.column]: cell.value}
                    if not res.get(cell.row):
                        res[cell.row] = obj
                    else:
                        res[cell.row].update(obj)
        return res

    @staticmethod
    def simple_horizontal_report(report_path, dict_list):
        '''
            @report_path: The filepath to store the report
            @dict_list: A list of dictionaries containing at most 1 list
            Returns: None, but generates an excel file containing columns equaling the keys of the dictionaries
        '''
        if not report_path or not dict_list: return None
        err_msg = 'list of dictionaries must not be empty.'
        err_msg_type = 'function takes a list of dictionaries as input.'
        err_msg_lists = 'dictionaries can have at most 1 list'
        if not len(dict_list): Excel.error_message(cf().f_code.co_name, err_msg, ValueError)
        if not isinstance(dict_list, dict): Excel.error_message(cf().f_code.co_name, err_msg_type, TypeError)
        list_count = 0
        sample = dict_list[0]
        fieldnames = []
        for key in sample.keys():
            if isinstance(sample.get(key), list): list_count += 1
            if list_count > 1: Excel.error_message(cf().f_code.co_name, err_msg_lists, AttributeError)
            fieldnames.append(key)

def test_excel_get_header_indicies():
    filename = 'printeron/pon_serverlist_master.xlsx'
    book = Excel.excel_open_book(filename)
    sheet = 'Polaris'
    indicies = Excel.excel_get_header_indicies(book, sheet, ['Host', 'Type'])
    print("Indicies:")
    print(indicies)

def test_excel_read_book():
    filename = 'printeron/pon_serverlist_master.xlsx'
    book = Excel.excel_open_book(filename)
    sheet = 'Polaris'
    data = Excel.excel_read_book(book, sheet, ['Host', 'Type'])
    print("Excel Data:")
    for idx, obj in data.items():
        print(idx)
        for field, value in obj.items():
            print(' - {}: {}'.format(field, value))

if __name__ == '__main__':
    #test_excel_get_header_indicies()
    test_excel_read_book()