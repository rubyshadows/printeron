# Pier Class
import requests
import json

DEBUG_LEVEL = 0

class Pier:
    url = 'http://technology.services.internal.t-mobile.com/SA/Services/TechService/TicketMgmtRestService.svc/'
    nt_user = 'NTMServiceUser'
    get_queries = {
        "user_tickets": url + "users/{}/tickets",    #user id
        "ticket": url + "tickets/{}/details",        #ticket id
        "worklogs": url + "tickets/{}/worklogs"      #ticket id
    }
    post_queries = {
        "add_ticket": url + "addticket",
        "add_worklog": url + "addworklog",
        "update_ticket": url + "updateticket",
        "update_ticket_basic": url + "updateticketbasic"
    }
    update_ticket_data = { # This hard coded JSON object is for reference for now
        "TicketDetails":{
            "Assignee":{
                "UserCode":"jjohnson",
                "UserName":"John Johnson"
            },
            "ModifiedBy":{
                "UserCode":"jjohnson",
                "UserName":"John Johnson"
            }
        }#,
        #"WorkLog":{
        #   "LogText":"Assigned to SOC",
        #   "CreatedBy":"John Johnson",
        #   "Auto_Created":'true'
        #}
    }

        
    @staticmethod
    def get_request(endpoint, args=[]):
        '''
            Executes the specified Pier API GET request with given argument
        '''
        req = Pier.get_queries[endpoint].format(*args)
        if DEBUG_LEVEL: print("Pier: {}".format(req))
        res = requests.get(req, verify = False)
        parsed = json.loads(res.text) # will be the json string turned into a python object # RETURN this
        #print (json.dumps(parsed, indent=2, sort_keys=True)) # printing the list of tickets
        if parsed.get("TicketDetail"):
            if len(parsed.get("TicketDetail")) > 0:
                #print (json.dumps(parsed.get("Tickets")[0], indent=2, sort_keys=True))
                pass
            else:
                #print ("No tickets found")
                pass
                
        return parsed
        
    @staticmethod
    def post_request(endpoint, args=[], data={}):
        '''
            Executes the specified Pier API POST request with given argument and data
        '''
        payload = json.dumps(data)
        headers = {'Content-Type': 'application/json'}
        req = Pier.post_queries[endpoint].format(*args)
        res = requests.post(req, headers=headers, data=payload, verify = False)
        parsed = json.loads(res.text)
        if DEBUG_LEVEL >= 3: print(json.dumps(parsed, indent=2, sort_keys=True))
        # print (json.dumps(parsed.get("Tickets")[0], indent=2, sort_keys=True))
        return parsed