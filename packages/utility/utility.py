from collections import OrderedDict, Counter
import csv
from inspect import currentframe as cf
import os

DEBUG_LEVEL = 0
SEP = os.sep

class Utility:
    ################ Map Functions ################

    @staticmethod
    def empty_map_list(key, dict_list):
        '''
            @key: key in each dict of dict_list
            @dict_list: list of dictionaries
            Returns: A dictionary of key:empty_list pairs
        '''
        my_map = {}
        for my_dict in dict_list:
            k = Utility.validate_key(cf().f_code.co_name, key, my_dict)
            my_map[k] = []
        return my_map

    @staticmethod
    def map(key, dict_list):
        '''
            @key: key in each dict of dict_list
            @dict_list: list of dictionaries
            Returns: A dictionary of information key'd by key
        '''
        if not key or not dict_list: return None
        my_map = {}
        for my_dict in dict_list:
            k = Utility.validate_key(cf().f_code.co_name, key, my_dict)
            my_map[k] = my_dict
        return my_map

    @staticmethod
    def map_class(key, obj_list):
        '''
            @key: attribute in each object
            @obj_list: list of objects
            Returns: A dictionary of objects key'd by key
        '''
        if not key or not obj_list: return None
        my_map = {}
        for my_obj in obj_list:
            k = Utility.validate_attr(cf().f_code.co_name, key, my_obj)
            if k not in my_map: my_map[k] = []
            my_map[k].append(my_obj)
        return my_map

    ############## Path Functions ################
    @staticmethod
    def abs_filepath(filename):
        '''
            Fix filepath
            @filename: relative filename to be turned absolute
            Returns: The absolute version of filename
        '''
        if not filename: return None
        new_filename = ''
        if Utility.pwd() not in filename:
            new_filename = Utility.pwd()+SEP+filename 
        else:
            new_filename = filename
        return new_filename

    @staticmethod
    def pwd():
        ''' Print directory of script
        '''
        #d = os.path.dirname(os.path.abspath(__file__))
        #print('pull_jira: script dir: {}'.format(d))
        w = os.getcwd()
        #print('pull_jira: working dir: {}'.format(w))
        '''for item in os.listdir(w):
            print(item)'''
        return w

    ############## File Functions ###############

    @staticmethod
    def base_filename(filename):
        return filename.split('.')[0]

    @staticmethod
    def empty_csv(filename, *fieldnames):
        '''
            Creates an empty csv with just a header
            @filename: name of the csv file
            @fieldnames: header of the csv
        '''
        entry_dict = {}
        for field in fieldnames:
            entry_dict[field] = None
        Utility.write_csv(filename, [entry_dict])

    @staticmethod
    def read_csv(filename):
        with open(filename, newline='', encoding='utf-8-sig') as csvfile:
            reader = csv.DictReader(csvfile)
            dict_list = []
            for row in reader:
                dict_list.append(row)
            return dict_list

    @staticmethod
    def to_csv(filename):
        return Utility.base_filename(filename) + '.csv'

    @staticmethod
    def write_csv(filename, dict_list, attempts=0, max_attempts = 20):
        '''
            Writes a list of dictionaries to a csv file
            @filename: name of the file to write to
            @dict_list: list of dictionaries to become rows in the csv
            @attempts: to be appended to filename if true file cannot be written to
            @max_attempts: number of times this will attempt to write the file
            Returns: None
            Throws: TypeError, ValueError
        '''
        func_name = cf().f_code.co_name
        err_empty = 'An empty list was passed in.'
        err_not_dict = 'This is not a list of dictionaries.'
        if not isinstance(dict_list, list): Utility.error_message(func_name, err_not_dict, TypeError)
        if not len(dict_list): Utility.error_message(func_name, err_empty, ValueError)
        if not isinstance(dict_list[0], (dict, OrderedDict)): Utility.error_message(func_name, err_not_dict, TypeError)
        fieldnames = dict_list[0].keys()
        filename = Utility.to_csv(filename)
        if attempts:
            base = Utility.base_filename(filename)
            filename = '{}({}).csv'.format(base, attempts)
        filename = Utility.abs_filepath(filename)
        try:
            with open(filename, 'w', newline='', encoding='utf-8-sig') as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                    writer.writeheader()
                    writer.writerows(dict_list)
        except PermissionError:
            if attempts < max_attempts:
                Utility.write_csv(filename, dict_list, attempts=attempts + 1)

    ############### Validation Functions ################
    @staticmethod
    def validate_key(func_name, key, my_dict):
        k = my_dict.get(key)
        msg = '{}: {} does not exist in {}'.format(func_name, key, my_dict)
        if not k: Utility.error_message(func_name, msg, KeyError)
        return k

    @staticmethod
    def validate_attr(func_name, attr, obj):
        msg = '{}: {} does not exist in {}'.format(func_name, attr, obj)
        try:
            k = getattr(obj, attr)
            return k
        except AttributeError:
            Utility.error_message(func_name, msg, AttributeError)


    @staticmethod
    def error_message(func_name, message, error_class):
        msg = '{}: {}'.format(func_name, message)
        raise error_class(msg)