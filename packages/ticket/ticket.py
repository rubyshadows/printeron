class Ticket:
    def __init__(self, ticket_id, assignee, description, status, created_date):
        self.ticket_id = ticket_id
        self.assignee = assignee
        self.description = description
        self.status = status
        self.created_date = created_date