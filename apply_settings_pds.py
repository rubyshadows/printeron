from bs4 import BeautifulSoup
import printeron.ponmanager_requests_login as pml
import json
from time import sleep



PDS_LINKS = {
    'prdasrmpr101': 'https://prdasrmpr101:8057/pds/0fefcc6a-0dd8-42ee-ace4-bb1cf76dfc3b/{}',
    'prdasrmpr102': 'https://prdasrmpr102:8057/pds/aa3b74d6-ff41-4f95-8c14-7e379060f2ea/{}',
    'prdpwrmpr0002':'https://prdpwrmpr0002:8057/pds/e9a5eef2-f1d2-4def-a721-de36062b5993/{}',
    'prdpwrmpr001d':'https://prdpwrmpr001d:8057/pds/7b0b6edc-d6a4-4cde-a5d4-1b224d6efc73/{}',
    'prdpwrmpr001e':'https://prdpwrmpr001e:8057/pds/1b2bcdf1-5ebc-49c5-a474-c6547a6dcbcc/{}',
    'prdpwrmpr001f':'https://prdpwrmpr001f:8057/pds/b2f67c87-2427-4b2f-a397-10fc0967ffcb/{}',
    'prdpwrmpr005d':'https://prdpwrmpr005d:8057/pds/a3f20dec-2ab1-41ff-ab63-0ea9a817db0f/{}',
    'prdpwrmpr005e':'https://prdpwrmpr005e:8057/pds/e2e4ec67-d397-4d38-bcad-650012c4621f/{}',
    'prdpwrmpr0060':'https://prdpwrmpr0060:8057/pds/a04e1d27-84e6-43ac-9ade-9acdf9c49881/{}',
    'prdpwrmpr0019':'https://prdpwrmpr0019:8057/pds/29f42f15-21ce-40e2-8c1a-816a55c6f5d8/{}',
    'prdpwrmpr0020':'https://prdpwrmpr0020:8057/pds/fbdb0653-4177-4bae-8192-7202d48c7230/{}',
    'prdpwrmpr0021':'https://prdpwrmpr0021:8057/pds/75f31f2c-8f1e-4f45-83fd-daded85bae7b/{}',
    'prdpwrmpr007f':'https://prdpwrmpr007f:8057/pds/ca5f6bea-f369-4af4-8474-c85c908c3250/{}',
    'prdpwrmpr0080':'https://prdpwrmpr0080:8057/pds/75e311ff-4194-4e30-b065-af0d39609ce6/{}',
    'prdpwrmpr0081':'https://prdpwrmpr0081:8057/pds/1cc3ca3e-3bf2-42e7-b321-d5511af01919/{}',
    'prdpwrmpr0082':'https://prdpwrmpr0082:8057/pds/4ed2c948-cc77-4351-823f-f65d1d574ec0/{}',
    'prdpwrmpr0083':'https://prdpwrmpr0083:8057/pds/b912c3bf-e257-46e3-b797-8951a3657d90/{}',
    'prdpwrmpr0084':'https://prdpwrmpr0084:8057/pds/839ed7e5-edfa-4f03-84ec-282ce7d068eb/{}',
    'prdasrmpr701':'https://prdasrmpr701:8057/pds/866d494b-d3f9-40b0-9748-10a01f71987e/{}',
    'prdasrmpr702':'https://prdasrmpr702:8057/pds/cb85065c-1975-4647-8ef9-31bc8be36170/{}',
    'prdtwrmpr0001':'https://prdtwrmpr0001:8057/pds/1e8620fd-1f96-42bc-9f4b-17107fd5355a/{}',
    'prdtwrmpr002d':'https://prdtwrmpr002d:8057/pds/75e6861b-c6a1-48ff-a181-badc80913158/{}',
    'prdtwrmpr002e':'https://prdtwrmpr002e:8057/pds/dea28e2d-585b-4f8a-92e6-55e3015917aa/{}',
    'prdtwrmpr002f':'https://prdtwrmpr002f:8057/pds/19cd86a1-92b8-4f74-b2c8-eec4736c7639/{}',
    'prdtwrmpr004d':'https://prdtwrmpr004d:8057/pds/631f7de5-ff4f-442a-949a-8d784a9e4392/{}',
    'prdtwrmpr004e':'https://prdtwrmpr004e:8057/pds/6ac8f926-0e01-4e54-8a98-42a0bb23b3ca/{}',
    'prdtwrmpr004f':'https://prdtwrmpr004f:8057/pds/5d6db0b6-d7fa-41f5-8ec2-0bb9c83c3bc3/{}',
    'prdtwrmpr0032':'https://prdtwrmpr0032:8057/pds/c4498f0c-7f6a-4eeb-8d8e-58b2b13514f9/{}',
    'prdtwrmpr0033':'https://prdtwrmpr0033:8057/pds/72d82447-416b-49f0-ad4f-752353e384a4/{}',
    'prdtwrmpr0034':'https://prdtwrmpr0034:8057/pds/d3c74493-fa7a-407a-9e89-fcbd96d32b40/{}',
    'prdtwrmpr002f':'https://prdtwrmpr002f:8057/pds/19cd86a1-92b8-4f74-b2c8-eec4736c7639/{}',
    'prdtwrmpr0098':'https://prdtwrmpr0098:8057/pds/d5fe48fb-a5f7-456d-bf2f-d3b71b36770c/{}',
    'prdtwrmpr0099':'https://prdtwrmpr0099:8057/pds/f7a97f07-3bd3-41c3-af3a-95cd8e99f113/{}',
    'prdtwrmpr009a':'https://prdtwrmpr009a:8057/pds/caafd782-ce38-46db-a877-bbbbc9309774/{}',
    'prdtwrmpr009b':'https://prdtwrmpr009b:8057/pds/863d4c15-b40e-4012-b4a4-c0f0fed3265a/{}',
    'prdtwrmpr009c':'https://prdtwrmpr009c:8057/pds/b3aa87f2-c201-4897-9642-ffd5dc916570/{}',
}

PRINT_PROCESSING = 'printProcessing/'
QUEUE_MONITOR = 'queuemonitor/ajax/?draw=1&columns%5B0%5D.data=jobId&columns%5B0%5D.name=&columns%5B0%5D.searchable=true&columns%5B0%5D.orderable=true&columns%5B0%5D.search.value=&columns%5B0%5D.search.regex=false&columns%5B1%5D.data=PTID&columns%5B1%5D.name=&columns%5B1%5D.searchable=true&columns%5B1%5D.orderable=true&columns%5B1%5D.search.value=&columns%5B1%5D.search.regex=false&columns%5B2%5D.data=documentName&columns%5B2%5D.name=&columns%5B2%5D.searchable=true&columns%5B2%5D.orderable=true&columns%5B2%5D.search.value=&columns%5B2%5D.search.regex=false&columns%5B3%5D.data=fileSize&columns%5B3%5D.name=&columns%5B3%5D.searchable=true&columns%5B3%5D.orderable=true&columns%5B3%5D.search.value=&columns%5B3%5D.search.regex=false&columns%5B4%5D.data=timeReceived&columns%5B4%5D.name=&columns%5B4%5D.searchable=true&columns%5B4%5D.orderable=true&columns%5B4%5D.search.value=&columns%5B4%5D.search.regex=false&columns%5B5%5D.data=printerName&columns%5B5%5D.name=&columns%5B5%5D.searchable=true&columns%5B5%5D.orderable=true&columns%5B5%5D.search.value=&columns%5B5%5D.search.regex=false&columns%5B6%5D.data=userEmail&columns%5B6%5D.name=&columns%5B6%5D.searchable=true&columns%5B6%5D.orderable=true&columns%5B6%5D.search.value=&columns%5B6%5D.search.regex=false&columns%5B7%5D.data=statusText&columns%5B7%5D.name=&columns%5B7%5D.searchable=true&columns%5B7%5D.orderable=true&columns%5B7%5D.search.value=&columns%5B7%5D.search.regex=false&columns%5B8%5D.data=pages&columns%5B8%5D.name=&columns%5B8%5D.searchable=true&columns%5B8%5D.orderable=true&columns%5B8%5D.search.value=&columns%5B8%5D.search.regex=false&order%5B0%5D.column=4&order%5B0%5D.dir=asc&start=0&length=10&search.value=&search.regex=false&_=1612952034120'
QUEUE_HTML_ID = 'queueMonitorTable_info'

'''
TODO:
1. Figure out how to post to the PDS servers, and then create a function which does it
2. Loop through to post to all of them
3. Add a delay parameter to the function
4. Add to the last sync executable
5. Fetch the number from the queue - Do this first for quick impact - and add it to the sync

<div class="dataTables_info" id="queueMonitorTable_info" role="status" aria-live="polite">Showing 1 to 10 of 988 entries</div>

<input type="hidden" name="rejectDuplicateJobs" value='true' autocomplete="off">
'''

def apply_pds(server):
    url = PDS_LINKS[server].format(PRINT_PROCESSING)
    client = pml.pon_login([server])
    token = pml.get_csrf(client, url)
    payload = {'_csrf': token,
                'rejectDuplicateJobs': 'true'}
    r = client.post(url, data = payload, headers=dict(Referer=url), timeout=360)
    return r

def apply_pds_all(stagger = 2):
    for server in PDS_LINKS:
        try:
            r = apply_pds(server)
            print('{}: {}'.format(server, r.status_code))
            sleep(stagger * 60)
        except Exception as e:
            print('{}: {}'.format(server, e))

def get_queue_count_all():
    result = {}
    print("Queue monitor count:\n")
    for server in PDS_LINKS:
        count = get_queue_count(server)
        print('{}: {}'.format(server, count))

def get_queue_count(server):
    client = pml.pon_login([server])
    url = PDS_LINKS[server].format(QUEUE_MONITOR)
    timeout = 10
    try:
        json_resp = client.get(url, timeout=timeout)
        json_data = json.loads(json_resp.text)
        count = json_data.get('recordsTotal')
        return count
    except Exception as e:
        print(e)
        return "Critical - Server not responding"


if __name__ == '__main__':
    r = apply_pds('prdasrmpr102')
    print(r.status_code)