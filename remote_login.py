# Install wmi https://pypi.org/project/WMI/

import wmi
from subprocess import Popen, PIPE
import re

from printeron.servers import PDS_SERVERS, CPS_SERVERS

def test():
    comp = wmi.WMI("PRDPWRMPR0011.gsm1900.org", user=r"GSM1900\MBurke16", 
                    password="Shilver13577!")
    c = comp
    billion = (1000000000, 'GB')
    million = (1000000, 'MB')
    thousand = (1000, 'KB')

    for i in comp.Win32_ComputerSystem():
        memory = int(i.TotalPhysicalMemory)
        print(round(memory / billion[0], 2), "GB total")

    pct_in_use = int([mem.PercentCommittedBytesInUse for mem in c.Win32_PerfFormattedData_PerfOS_Memory()][0])
    print(str(pct_in_use) + '%', "usage")

    available_mbytes = int([mem.AvailableMBytes for mem in c.Win32_PerfFormattedData_PerfOS_Memory()][0])
    print(available_mbytes, 'MB available')

###################### Low Level Functions ########################

def connect(computer=None, user=None, password=None, conn=None):
    if conn:
        return conn
    if computer and user and password:
        c = wmi.WMI(computer=computer, user=user, password=password, find_classes=False)
        return c
    return None


def get_uptime(computer=None, user=None, password=None, conn=None):
    c = connect(computer, user, password, conn)
    if not c:
        print("get_uptime: No parameters provided")
        return None

    secs_up = int([uptime.SystemUpTime for uptime in c.Win32_PerfFormattedData_PerfOS_System()][0])
    hours_up = secs_up / 3600
    return round(hours_up, 2)


def get_cpu(computer=None, user=None, password=None, conn=None):
    c = connect(computer, user, password, conn)
    if not c:
        print("get_cpu: No parameters provided")
        return None

    utilizations = [cpu.LoadPercentage for cpu in c.Win32_Processor()]
    utilization = int(sum(utilizations) / len(utilizations))  # avg all cores/processors
    return round(utilization, 2)

    
def get_mem_mbytes(computer=None, user=None, password=None, conn=None):
    c = connect(computer, user, password, conn)
    if not c:
        print("get_mem_mbytes: No parameters provided")
        return None

    available_mbytes = int([mem.AvailableMBytes for mem in c.Win32_PerfFormattedData_PerfOS_Memory()][0])
    return available_mbytes


def get_mem_pct(computer=None, user=None, password=None, conn=None):
    c = connect(computer, user, password, conn)
    if not c:
        print("get_mem_pct: No parameters provided")
        return None

    [print(mem) for mem in c.Win32_PerfFormattedData_PerfOS_Memory()]
    print('-------------------------------')
    [print(mem) for mem in c.Win32_Processor()]

    pct_in_use = int([mem.PercentCommittedBytesInUse for mem in c.Win32_PerfFormattedData_PerfOS_Memory()][0])
    return pct_in_use


def get_mem_total(computer=None, user=None, password=None, conn=None):
    c = connect(computer, user, password, conn)
    if not c:
        print("get_mem_total: No parameters provided")
        return None

    billion = 1000000000
    for i in c.Win32_ComputerSystem():
        memory = int(i.TotalPhysicalMemory)
        return round(memory / billion, 2)
    
 
def ping(host_name):
    # Broken
    p = Popen('ping -n 1 ' + host_name, stdout=PIPE)
    m = re.search('Average = (.*)ms', p.stdout.read())
    if m:
        return True
    else:
        raise Exception

############################# High Level Commands ##############################

def report_all_servers(ntid, pw):
    '''
        Reports CPU, Memory, Uptime, and Service Status of all 72 servers 
        TODO:
            1. Create two loops, to iterate through each group of servers; CPS and PDS.
            2. Login to a server
            3. Run every command for that server 
            4. Print results 
            5. Login to next server

        Analysis: 72 logins 
    '''
    print('CPS server status\n')
    count = 0
    for cps in CPS_SERVERS:
        count += 1
        server = cps + '.gsm1900.org'
        c = connect(server, user="GSM1900\\" + ntid, password=pw)
        uptime = get_uptime(conn=c)
        cpu = get_cpu(conn=c)
        mtotal = get_mem_total(conn=c)
        mbytes = get_mem_mbytes(conn=c)
        pct = get_mem_pct(conn=c)
        print(server)
        print('uptime:', str(uptime), 'hours')
        print('cpu:', str(cpu) + '%')
        print('total memory:', str(mtotal), 'GB')
        print('memory available:', str(mbytes), 'MB')
        print('memory usage: ',str(pct) + '%')
        print()
        if count == 2:
            break

if __name__ == '__main__':
    report_all_servers('MBurke16', 'Shilver13577!')