import sys
import requests
import json
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Resources
# User vs System variables for python modules: https://stackoverflow.com/questions/4477660/what-is-the-difference-between-user-variables-and-system-variables
# CSRF token usage with Python: https://stackoverflow.com/questions/13567507/passing-csrftoken-with-python-requests
# Why CSRF is in meta tag not cookie: https://stackoverflow.com/questions/21473515/why-csrf-token-should-be-in-meta-tag-and-in-cookie

PRINTERS_URL = 'https://{}:8057/printers/'
URL = 'https://{}:8057/login'
AJAX = 'https://{server}:8057/printers/list/ajax?draw=4&start=0&length=10&search.value={string}&search.regex=false&printerClass=%5BPUBLIC%2C+IPP%5D&_=1596161211848'
AJAX_PDS = 'https://{server}:8057/pds/{pds_id}/printers/list?draw=4&start=0&length=10&search.value={string}&search.regex=false&printerClass=%5BPUBLIC%2C+IPP%5D&_=1596161211848'
USERNAME = 'root'
PASSWORD = 'Pr1nTer0n!!'
SERVERS = ['prdtwrmpr002a', 'prdasrmpr001', 'prdpwrmpr001a', 'prdpwrmpr005a', 
           'prdpwrmpr0010', 'prdpwrmpr006d', 'prdpwrmpr0070',
           'prdasrmpr601', 'prdtwrmpr004a',
           'prdtwrmpr0022', 'prdtwrmpr0085', 'prdtwrmpr0088']
PDS_SERVERS = {'prdtwrmpr002a': 'dea28e2d-585b-4f8a-92e6-55e3015917aa', 
                'prdasrmpr001': 'aa3b74d6-ff41-4f95-8c14-7e379060f2ea', 
                'prdpwrmpr001a': '1b2bcdf1-5ebc-49c5-a474-c6547a6dcbcc',
                'prdpwrmpr005a': 'a3f20dec-2ab1-41ff-ab63-0ea9a817db0f', 
                'prdpwrmpr0010': None, 'prdpwrmpr006d': None, 'prdpwrmpr0070': None,
                'prdasrmpr601': None, 'prdtwrmpr004a': None,
                'prdtwrmpr0022': None, 'prdtwrmpr0085': None, 'prdtwrmpr0088': None}
PROXIES = { 
          'http'  : "proxy.internal.t-mobile.com:9090", 
          'https' : "proxy.internal.t-mobile.com:9090"
        }

class Login_Failure(Exception):
    pass

def get_csrf(client, url):
    r = client.get(url, allow_redirects=True, verify=False, timeout=10)  # sets cookie
    #print('retrieved csrf token')
    html = r.text
    mark_start = '<meta name="_csrf" content="'
    mark_end = '"/>'
    # index of those two points
    start_index = html.find(mark_start) + len(mark_start)
    end_index = html.find(mark_end, start_index)
    # and text between them is our token, store it for second step of actual login
    csrftoken = html[start_index:end_index]
    return csrftoken

def pon_login(servers = SERVERS, index = 0):

    client = requests.session()
    server = servers[index]
    url = URL.format(server)
    #print('url: {}'.format(url))

    # Retrieve the CSRF token first
    r = client.get(url, allow_redirects=True, verify=False, timeout=10)  # sets cookie
    #print('retrieved csrf token')
    html = r.text
    mark_start = '<meta name="_csrf" content="'
    mark_end = '"/>'
    # index of those two points
    start_index = html.find(mark_start) + len(mark_start)
    end_index = html.find(mark_end, start_index)
    # and text between them is our token, store it for second step of actual login
    csrftoken = html[start_index:end_index]
    #print('_csrf: {}'.format(csrftoken))

    login_data = dict(username=USERNAME, password=PASSWORD, _csrf=csrftoken, next='/')
    r = client.post(url, data=login_data, headers=dict(Referer=url))

    contents = r.text
    check_string = 'logout'
    index = contents.find(check_string)
    # if we find it
    if index != -1:
        #print(f"We found '{check_string}' at index position : {index}")
        #print('Login successful: {}'.format(server))
        #print('###################################')
        return client
    else:
        print(f"String '{check_string}' was not found! Maybe we did not login ?!")
        print('###################################')
        print()
        print(contents)
        return None

def ajax_query(client, server, string, retry = False):
    try:
        #print(AJAX.format(string=string, server=server))
        #json_data = client.get(AJAX.format(string=string, server=server))
        pds_id = PDS_SERVERS.get(server)
        json_data = client.get(AJAX_PDS.format(string=string, server=server, pds_id=pds_id))
    except:
        print('ajax_query: session timed out')
        return None
    text = json_data.text
    try:
        return json.loads(text)
    except:
        # log failure
        raise Login_Failure('ajax query failure: {}'.format(string))

if __name__ == '__main__':
    client = pon_login()
    test1 = ajax_query(client, '123456')
    test2 = ajax_query(client, '7983')

# TODO
# 1. Navigate to the printers url (done)
# 2. Post a test query to the AJAX Search url (done)
    # - Try a valid one
    # - Try one that brings back too many printers 
    # - Try one that brings back no printers 
# 3. Retrieve the entries (done)
# 4. Save to a printer object (mostly done - needs designing)
    # - Push to JSON store for UI demo (later)
# 5. Create a console that takes queries 
    # - Attempt login when app boots up
    # - Re-login on failure
        # - both requires a global client session variable
    # - Stay logged in throughout the app's life
# 6. Create a UI with a search field 
    # Just read from JSON for now and show team in the morning
        # Show them that I can search for printers using the UI

# Considerations:
# 1. Reusability / Modularity:
#   - I want a login session with multiple servers 
# 2. Repeatability:
#   - I want to use functions multiple times on one login
#   - TODO: Seperate login into its own function
#   - TODO: AJAX Query will be its own function
# 3. Status Data:
#   - I want to know the last-sync status of all printers 
#   - I can decide which ones to sync based on that
#   - Requires I log into all servers on a continuous basis
# 4. Durability:
#   - If the app fails, I don't want it to hold things up:
#       - Fails at logging into a server: Retry, Mark as fail, Move on
#       - Asynchronous architecture would be ideal
#           - At least Flask will partition requests asynchronously