import cmd
import printeron.ponmanager_requests_login as pml
import printeron.sync_collector as sync_collector
import printeron.apply_settings as apply_cps
import printeron.apply_settings_pds as apply_pds

#pyinstaller -F printeron\ponmanager_console.py

class FASTPrint(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.prompt = ("(FASTPrint) ")
        self.session = None
        self.servers = pml.SERVERS
        self.server_idx = 0
        self.setup()
        self.onecmd("?")

    def setup(self):
        try:
            self.session = pml.pon_login(index = self.server_idx)
        except Exception as e:
            print('Login failed for {} - {}'.format(self.servers[self.server_idx],e))
            self.server_idx += 1
            if self.server_idx == len(self.servers):
                self.server_idx = 0
                self.session = None
                return
            self.setup()

    def do_quit(self, arg):
        '''
            Quits program
        '''
        return True

    def do_last_sync(self, arg):
        '''
            Reads when each cps server was synced
            Usage: last_sync
        '''
        all_servers = sync_collector.extract_all_last_syncs()
    
    def do_pds_queue(self, arg):
        '''
            Reads the queue monitor for all PDS servers
            Usage: pds_queue
        '''
        apply_pds.get_queue_count_all()

    def do_sync_cps(self, arg):
        '''
            Applies settings to all CPS servers 
            Usage: sync_cps <number of minutes to stagger on - default is 2>
        '''
        args = self.parse(arg)
        if args: stagger = args[0]
        else: stagger = 2

        print('SYNCING CPS SERVERS\n')
        apply_cps.sync_all(stagger)

    def do_sync_pds(self, arg):
        '''
            Applies settings to all CPS servers 
            Usage: sync_pds <number of minutes to stagger on - default is 2>
        '''
        args = self.parse(arg)
        if args: stagger = args[0]
        else: stagger = 2

        print('SYNCING PDS SERVERS\n')
        apply_pds.apply_pds_all(stagger)


    def do_sync_all(self, arg):
        '''
            Applies settings to all CPS and PDS servers
            Usage: sync_all <number of minutes to stagger on - default is 2>
        '''
        args = self.parse(arg)
        if args: stagger = args[0]
        else: stagger = 2

        print('SYNCING CPS SERVERS\n')
        apply_cps.sync_all(stagger)
        print()
        print('SYNCING PDS SERVERS\n')
        apply_pds.apply_pds_all(stagger)

    def parse(self, arg):
        'Convert a series of zero or more numbers to an argument tuple'
        return tuple(map(float, arg.split()))

    # def do_sync_update(self, arg): # Has a stagger minutes argument - default 2 minutes

    # def do_search(self, arg):
    #     '''
    #         Query CPS server for matching printers
    #         Usage: search <search string>
    #     '''
    #     if arg: self.search(arg)

    def emptyline(self):
        pass

    def search(self, arg):
        try:
            printers = pml.ajax_query(self.session, self.servers[self.server_idx], arg)
            print(printers)
            if not printers or not printers.get('data'):
                self.server_idx += 1
                if self.server_idx == len(self.servers):
                    self.server_idx = 0
                    self.setup()
                    print('Checked all servers')
                    return 
                self.setup()
                if not self.session: 
                    print('search: Having trouble logging into servers')
                    return
                return self.search(arg)
            return printers
        except pml.Login_Failure:
            self.setup()


if __name__ == "__main__":
    '''
        Entry point for the loop.
    '''
    FASTPrint().cmdloop()