from datetime import datetime
from pytz import timezone, utc
import requests
from time import sleep

import printeron.ponmanager_requests_login as pml
import printeron.sync_collector as sync
import pyaml

URL = 'https://{}:8057/status/generalSettings'
test = {
    # SERVER_SETTINGS = {
    #     "loggingModel":{"logLevel":"INFO"}, 
    #     "jobManagementModel":{"maximumJobSize":100, "pendingReleaseJobExpiry":50, "pendingDownloadJobExpiry": 72}, 
    #     "_proxyModel":{"enableProxy": "on"}, 
    #     "proxyModel":{"proxyURL": "http://proxy.fastautomation.com", 
    #                   "proxyPort": 80, "proxyUser": "proxyadmin",
    #                   "proxyPassword": "password"}, 
    #     "synchronizationSettingsModel":{"enableCPSSync":True,
    #                                     "enableCPSAutoSync": True,
    #                                     "syncIntervalCPS":2,
    #                                     "syncIntervalPDG": 5,
    #                                     "enablePDSSync": True,
    #                                     "enablePDSAutoSync": True,
    #                                     "syncIntervalPDS": 480,},
    #     "_synchronizationSettingsModel":{"enableCPSSync":"on",
    #                                     "enableCPSAutoSync": "on",
    #                                     "enablePDGSync":"on",
    #                                     "enablePDSSync": "on",
    #                                     "enablePDSAutoSync": "on",},
    #     "_serviceCapabilitiesModel":{"preview":"on", "sra":"on",
    #                                  "dirSearch": "on", "ponUsers": "on",
    #                                  "docApi": "on"},
    #     "serviceCapabilitiesModel":{"dirSearch": True, "docApi": True,
    #                                 "companyName": "PrinterOn",
    #                                 "maxFileSize": 100,
    #                                 "connectionType":"PROXY"}, 
    #     "_csrf":"",
    # }
}
SERVER_SETTINGS = {
    "proxyModel.proxyURL": "http://proxy.fastautomation.com",
    "_csrf":"",
}

# Login to a server
# Visit url, and get the csrf token for that server
# Post to the URL
# Check to make sure it updated

# parent server: https://prdpwrmpr001a:8057/status/generalSettings
# child server: https://prdpwrmpr001b:8057/status/generalSettings
# considerations: response time differs with different servers
# solution: try to wait a minimum amount of time. time it, and wait the difference if it's less than X

def get_pst_time():
    date_format='%m_%d_%Y_%H_%M_%S_%Z'
    date = datetime.now(tz=utc)
    date = date.astimezone(timezone('US/Pacific'))
    pstDateTime=date.strftime(date_format)
    return pstDateTime

def step_1_login(server):
    '''
        Description: Login to a server and return session client
        Input: Server
        Output: Session Client
    '''
    return pml.pon_login([server])

def step_2_csrf_token(client, url):
    '''
        Description: Grab the csrf token from the url
        Input: Client, Url
        Output: CSRF token
    '''
    return pml.get_csrf(client, url)

def step_3_post_settings(client, url, csrf):
    '''
        Description: Apply settings to server
        Input: Client, Url, CSRF Token
        Output: Returned status code
    '''
    time = get_pst_time()
    SERVER_SETTINGS['_csrf'] = csrf
    SERVER_SETTINGS['proxyModel.proxyURL'] = 'fast automation {}'.format(time)
    r = client.post(url, data = SERVER_SETTINGS, headers=dict(Referer=url), timeout=360)
    return r

def sync_all(stagger = 2, servers = sync.SERVERS):
    '''
        Applies settings to all servers
    '''
    for server in servers:
        url = URL.format(server)

        try:
            client = step_1_login(server)
            token = step_2_csrf_token(client, url)
            response = step_3_post_settings(client, url, token)
            print('{}: {}'.format(server, response.status_code))
            sleep(stagger * 60)
        except Exception as e:
            print('{}: {}'.format(server, e))
        


if __name__ == '__main__':
    sync_all(1)
    #print(response.text)

'''
TODO:
1. Run this for all the CPS servers.
- I should be able to just for loop through them 
2. Make a function that works for PDS servers
- Write them down and for loop through them
3. Add to the last sync executable
'''