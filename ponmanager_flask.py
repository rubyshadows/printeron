from flask import Flask, render_template
import printeron.ponmanager_requests_login as pml
import printeron.ponmanager_console as pc
import printeron.sync_collector as sync_collector

def create_app(testing = True):
    app = Flask(__name__)
    app.url_map.strict_slashes = False
    fastprint = pc.FASTPrint()

    @app.route('/')
    def landing_page():
        return render_template("index.html")

    @app.route('/search/<text>')
    def search(text):
        printers = fastprint.search(text)
        if not printers:
            return "No results found"
        data = printers.get('data')
        total = printers.get('recordsFiltered')
        return render_template("results.html",data=data, total=total)

    @app.route('/last_sync')
    def last_sync():
        all_servers = sync_collector.extract_all_last_syncs()
        #print('Complete\n###################################\n')
        #print('all servers: {}'.format(all_servers))
        return render_template("last_sync.html", data=all_servers)

    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000)